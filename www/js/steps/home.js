var previousCardUsed = false;
var lastCardNotOrderedId = null;

function getLastCards() {
    $("#previous_cards").html("");
    
    app.storage.getCards(function(cards) {
        var l = (cards.length > 4) ? 4 : cards.length;
        
        for (var i = 0; i < l; i++) {
            var card = cards[cards.length - 1 - i];
            
            var imageUrl = "http://posten.dinavykort.se/mobile_v2/previousCards/" + card.galleryId + ".jpg";
            
            var context = {
                zIndex: l - i,
                id: "previous_card_" + card.cardId,
                class: "previous_card_background",
            }
            
            var item = HandlebarTemplates.previousCardTpl(context);
            
            $("#previous_cards").append(item);
            
            var image = new Image();
            
            image.src = imageUrl;
            
            image.onload = (function(id) {
                return function() {
                    $("#previous_card_" + id).parent().find(".previous_card_loader").remove();
                    
                    var canvas = document.getElementById("previous_card_" + id);
                    
                    var ctx = canvas.getContext("2d");
                    
                    ctx.drawImage(this, 0, 0, canvas.width, canvas.height);
                }
            })(card.cardId);
        }
        
        $("#previous_cards").css({
            "width": (72 * l) + "px",
            "padding-left": (6 * l) + "px",
        });
    });
}

function getPreviousCards() {
    $("#previous_cards_list").html("");
    
    app.storage.getCards(function(cards) {
        for (var i = 0; i < cards.length; i++) {
            var card = cards[cards.length - 1 - i];
            
            if (card.ordered === true) {
                var imageUrl = "http://posten.dinavykort.se/mobile_v2/previousCards/" + card.galleryId + ".jpg";
                
                var cardImageContext = {
                    zIndex: cards.length - i,
                    id: "previous_card_image_" + card.cardId,
                    class: "previous_card_image",
                }
                
                var orderedDate = new Date(card.orderedDate);

                var listItemContext = {
                    zIndex: cards.length - i,
                    id: card.cardId,
                    orderedDate: orderedDate.getDate() + "." + orderedDate.getMonth() + ". - " + orderedDate.getFullYear(),
                    text: card.text,
                    class: "previous_card_list_item",
                }
                
                var cardImageItem = HandlebarTemplates.previousCardTpl(cardImageContext);
                
                var listItem = HandlebarTemplates.previousCardListItemTpl(listItemContext);

                $("#previous_cards_list").append(listItem);

                $("#previous_cards_list").find("#previous_card_list_item_" + card.cardId).prepend(cardImageItem);

                var image = new Image();
                
                image.src = imageUrl;
                
                image.onload = (function(id) {
                    return function() {
                        $("#previous_card_image_" + id).parent().find(".previous_card_loader").remove();
                        
                        var canvas = document.getElementById("previous_card_image_" + id);
                        
                        var ctx = canvas.getContext("2d");
                        
                        ctx.drawImage(this, 0, 0, canvas.width, canvas.height);
                    }
                })(card.cardId);
                
            }
        }
    });
}

function getLastCardNotOrdered() {
    app.storage.getLastCardNotOrdered(function(card) {
        if (card != null) {
            if (card.ordered === false && card.galleryId != null) {
                var imageUrl = "http://posten.dinavykort.se/mobile_v2/previousCards/" + card.galleryId + ".jpg";

                var cardImageContext = {
                    zIndex: 99,
                    id: "previous_card_image_" + card.cardId,
                    class: "last_card_not_uploaded",
                }
                
                var orderedDate = new Date(card.orderedDate);

                var listItemContext = {
                    zIndex: 99,
                    id: card.cardId,
                    orderedDate: orderedDate.getDate() + "." + orderedDate.getMonth() + ". - " + orderedDate.getFullYear(),
                    infoText: "Ikke sendt!",
                    text: card.text,
                    class: "last_card_not_uploaded_item",
                }
                
                var cardImageItem = HandlebarTemplates.previousCardTpl(cardImageContext);
                
                var listItem = HandlebarTemplates.previousCardListItemTpl(listItemContext);

                $("#previous_cards_list").prepend(listItem);

                $("#previous_cards_list").find("#previous_card_list_item_" + card.cardId).prepend(cardImageItem);
                
                var image = new Image();
                
                image.src = imageUrl;
                
                image.onload = (function(id) {
                    return function() {
                        $("#previous_card_image_" + id).parent().find(".previous_card_loader").remove();
                        
                        var canvas = document.getElementById("previous_card_image_" + id);
                        
                        var ctx = canvas.getContext("2d");
                        
                        ctx.drawImage(this, 0, 0, canvas.width, canvas.height);
                    }
                })(card.cardId);
            }
        }
    });
}

function getPreviousCard(card) {
    functions.showLoader("Laster postkort fra galleriet");
    
    app.card.cardType = "";
    app.card.numberOfImages = 1;
    app.card.images = [];
    
    var image = new Image();
    
    image.src = "http://posten.dinavykort.se/gallery/org" + card.galleryId + ".jpg";
    //alert("http://posten.dinavykort.se/gallery/org" + card.galleryId + ".jpg");
    
    //image.src = "http://posten.dinavykort.se/mobile_v2/previousCards/" + card.galleryId + ".jpg";
    
    image.onload = (function() {
        return function() {
            $("#preview").show();
            
            var canvas = document.createElement("canvas");
            
            canvas.id = "cloned2_image_1";
            canvas.width = image.width;
            canvas.height = image.height;
            
            var $container = $("<div/>").css({
                "width": "300px",
                "height": "213px",
                "position": "absolute",
                "top": "8px",
                "left": "8px",
            });
            
            $container.append(canvas);
            
            $("#preview_card_container .card_container").html("").append($container);
            
            //$("#cloned2_image_1").attr("width", "300").attr("height", "213");
            
            var ctx = canvas.getContext("2d");
            
            ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
            
            //$("#cloned2_image_1").attr("width", "300").attr("height", "213");
            
            $("#cloned2_image_1").css({
                "width": "300px",
                "height": "213px",
            });
            
            $("#nav_card_btn").data("disabled", true);
            
            $("#preview").hide();
            
            setCurrentSubStep("write_text");
            
            app.currentSubStep.previousStepName = "home";
            
            showStep(app.currentSubStep);
            
            previousCardUsed = true;
            
            functions.hideLoader();
        }
    })();

    var imageObject = new ImageObject();
    
    imageObject.imageURI = image.src;
    imageObject.imageNumber = 1;
    imageObject.zoomAmount = .6;
    imageObject.slider = 0;
    imageObject.offsetX = 160;
    imageObject.offsetY = 145;
    imageObject.canvasWidth = 300;
    imageObject.canvasHeight = 213;
    imageObject.posX = 0;
    imageObject.posY = 0;
    imageObject.editCanvasWidth = 300;
    imageObject.editCanvasHeight = 213;
    imageObject.frameWidth = 300;
    imageObject.frameHeight = 213;
    imageObject.frameOffsetX = 0;
    imageObject.frameOffsetY = 0;
    
    app.card.images[0] = imageObject;
    
    currentImageURI = image.src;
    
    $(".card_overlay").hide();
    
    changeFont(card.fontId);
    changeFontColor(card.fontColorId);
    changeFontSize(card.fontSize);
    changeTextAlign(card.textAlign);
    
    updateText(card.text);
}

$("#new_card_btn").bind('touchend', function() {
    if (previousCardUsed === true) {
        var currentRebate = app.card.rebate;
        
        app.card = new Card();
        
        app.card.rebate = currentRebate;

        changeFont(app.card.fontId);
        changeFontColor(app.card.fontColorId);
        changeFontSize(app.card.fontSize);
        changeTextAlign(app.card.textAlign);
        
        updateText(app.card.text);
        updateAddressInfo();
        
        app.steps[2].subSteps[0].previousStepName = null;
        
        disableNavButtons(1);
    }
    else {
        previousCardUsed = false;
    }
    
    setCurrentSubStep("card");
    
    showStep(app.currentSubStep, "right", true);
});

$("#previous_cards").on('touchend', ".previous_card_background", function() {
    var cardId = $(this).find("canvas").attr("id").split("_")[2];
        
    app.storage.getCardById(cardId, function(card) {
        getPreviousCard(card);
    });
});

$("#previous_cards_btn").bind('touchend', function() {
    getPreviousCards();
    
    getLastCardNotOrdered();
    
    showOverlay("previous_cards_list");
});

$("#previous_cards_list").on('touchend', ".previous_card_list_item", function() {
    if (scrolling === false) {
        var cardId = $(this).attr("id").split("_")[4];
        
        app.storage.getCardById(cardId, function(card) {
            getPreviousCard(card);
        });
    }
});

$("#previous_cards_list").on('touchend', ".last_card_not_uploaded_item", function() {
    app.storage.getLastCardNotOrdered(function(card) {
        lastCardNotOrderedId = card.cardId;
        
        app.card.cardId = card.cardId;
        app.card.orderId = card.orderId;
        app.galleryId = card.galleryId;
        
        getPreviousCard(card);
    });
});

