function checkIfDateIsHoliday(date) {
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();
    
    // New year
    if (month == 1 && day == 1)
        return true;
    
    // First of may
    if (month == 5 && day == 1)
        return true;
    
    // National day
    if (month == 5 && day == 17)
        return true;
    
    // Christmas
    if (month == 12 && (day == 26 || day == 25))
        return true;
    
    // Non-regular days (update when necessary)
    if (year == 2013) {
        if (month == 3) {
            if (day == 24 || day == 28 || day == 29 || day == 31)
                return true;
        }
        else if (month == 5) {
            if (day == 9 || day == 19 || day == 20)
                return true;
        }
    } else if (year == 2014) {
        if (month == 4) {
            if (day == 13 || day == 17 || day == 18 || day == 20 || day == 21)
                return true;
        }
        else if (month == 5) {
            if (day == 29)
                return true;
        }
        else if (month == 6) {
            if (day == 8 || day == 9)
                return true;
        }
    }
    else if (year == 2015) {
        if (month == 2) {
            if (day == 15)
                return true;
        }
        else if (month == 3) {
            if (day == 22)
                return true;
        }
        else if (month == 4) {
            if (day == 2 || day == 3 || day == 5)
                return true;
        }
        else if (month  == 5) {
            if (day == 14 || day == 24 || day == 25)
                return true;
        }
    }
    else if (year == 2016) {
        if (month == 3) {
            if (day == 24 || day == 25 || day == 28)
                return true;
        }
        else if (month == 5) {
            if (day == 5 || day == 16 || day == 17)
                return true;
        }
    }
    
    // If we got here the date is not a holiday
    return false;
}

function getNameOfMonth(month) {
    var monthNames = new Array("JANUAR", "FEBRUAR", "MARS", "APRIL", "MAI", "JUNI", "JULI", "AUGUST", "SEPTEMBER", "OKTOBER", "NOVEMBER", "DESEMBER");
    return monthNames[month];
}

var calendarDate = null;
var selectedDate = null;
var earliestDeliveryDate = null;

var today = new Date();

function calendarUpdate(date) {
    calendarDate = date;
    
    $("#calendar_month").text(getNameOfMonth(date.getMonth()) + " " + date.getFullYear());
    
    var daysInMonth = new Date(date.getFullYear(), date.getMonth() + 1, 0).getDate();

    var weekday = new Date(date.getFullYear(), date.getMonth(), 1).getDay();

    if (weekday == 0)
        weekday = 7;

    $("#calendar_table").find("tr:gt(0)").remove();

    var html = '<tr>';

    for (var i = 1; i < weekday; i++) {
        html += '<td class="calendar_date_inactive"></td>';
    }

    var firstSelectableDate = true;

    var nextMonday = new Date();
    nextMonday.setDate(nextMonday.getDate() + (nextMonday.getDay() + 7) % 7 + 1);

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    for (var i = 1; i <= daysInMonth; i++) {
        var loopDate = new Date(date.getFullYear(), date.getMonth(), i);
        
        weekday = loopDate.getDay();
        
        if (weekday == 0) {
            html += '<td id="' + loopDate + '" class="calendar_date_inactive">' + i + '</td></tr><tr>';
        }
        else {
            if (today.getDay() == 5 && loopDate == nextMonday) {
                html += '<td id="' + loopDate + '" class="calendar_date_inactive">' + i + '</td>';
            }
            else if (weekday >= 5 && loopDate == nextMonday) {
                html += '<td id="' + loopDate + '" class="calendar_date_inactive">' + i + '</td>';
            }
            else if (loopDate <= tomorrow) {
                html += '<td id="' + loopDate + '" class="calendar_date_inactive">' + i + '</td>';
            }
            else if (checkIfDateIsHoliday(loopDate)) {
                html += '<td id="' + loopDate + '" class="calendar_date_inactive">' + i + '</td>';
            }
            else {
                if (firstSelectableDate) {
                    earliestDeliveryDate = loopDate;
                    
                    firstSelectableDate = false;
                }
                
                if (selectedDate != null) {
                    selectedDate.setHours(0, 0, 0, 0);
                    loopDate.setHours(0, 0, 0, 0);
                    
                    if (selectedDate.getTime() == loopDate.getTime()) {
                        html += '<td id="' + loopDate + '" class="calendar_date_valid calendar_date_selected">' + i + '</td>';
                    } else {
                        html += '<td id="' + loopDate + '" class="calendar_date_valid">' + i + '</td>';
                    }
                } else {
                    if (earliestDeliveryDate.getTime() == loopDate.getTime()) {
                        html += '<td id="' + loopDate + '" class="calendar_date_valid calendar_date_selected">' + i + '</td>';
                        
                        selectedDate = earliestDeliveryDate;
                    }
                    else {
                        html += '<td id="' + loopDate + '" class="calendar_date_valid">' + i + '</td>';
                    }
                }
            }
        }
    }
    
    weekday = new Date(date.getFullYear(), date.getMonth(), daysInMonth).getDay();

    if (weekday != 0) {
        while (weekday <= 6) {
            html += '<td class="calendar_date_inactive"></td>';
            
            if (weekday == 6)
                html += '</tr><tr>';
            
            weekday++;
        }
    }
    
    $("#calendar_table").append(html);
}

$("#calendar_left_btn").bind('touchstart', function () {
    calendarDate.setMonth(calendarDate.getMonth() - 1);
    
    calendarUpdate(calendarDate);
});

$("#calendar_right_btn").bind('touchstart', function () {
    calendarDate.setMonth(calendarDate.getMonth() + 1);
    
    calendarUpdate(calendarDate);
});

$("#calendar_table").on('touchstart', "td", function () {
    if ($(this).hasClass("calendar_date_valid")) {
        $(".calendar_date_selected").removeClass("calendar_date_selected");
        
        $(this).addClass("calendar_date_selected");
        
        selectedDate = new Date($(this).attr("id"));
    }
});

function updateBasket() {
    $(".basket_info").text("POSTKORT x " + app.card.address.length + ", LEVERES " + selectedDate.getDate() + "." + (selectedDate.getMonth() + 1) + "." + selectedDate.getFullYear().toString().substr(2,2));
    
    var price = app.card.address.length * 29;
    
    if (app.card.rebate !== null) {
        price = price - app.card.rebate.cardsLeft * 29;
        
        if (price < 0)
            price = 0;
    }
    
    app.card.price = price;
    
    $(".basket_price").text(price + ",00");
}

function updatePaymentInfo() {
    if (app.card.rebate != null && app.card.address.length <= app.card.rebate.cardsLeft) {
        if (app.card.rebate.cardLimit > 1) {
            $("#payment_info").text("Dine " + app.card.rebate.cardLimit + " første postkort er gratis. Deretter koster tjenesten kr 29.- per kort.");
        }
        else {
            $("#payment_info").text("Ditt første postkort er gratis. Deretter koster tjenesten kr 29.- per kort.");
        }
        
        $("#payment_rebate_code").val(app.card.rebate.code).show();
        
        if (app.card.rebate.layout != null) {
            $(".card_background").css("background", "url('img/rebate_layouts/backside_bg_" + app.card.rebate.id + ".png') no-repeat");
        }
        else {
            $(".card_background").css("background", "url('img/backside_bg.png') no-repeat");
        }
        
        $("#one_image_btn").css("background", "url('img/one_image_free_btn.png') no-repeat");
        $("#multiple_images_btn").css("background", "url('img/multiple_images_free_btn.png') no-repeat");
    }
    else if (app.card.rebate != null && app.card.rebate.layout != null) {
        $("#payment_rebate_code").val(app.card.rebate.code).show();
        $(".card_background").css("background", "url('img/rebate_layouts/backside_bg_" + app.card.rebate.id + ".png') no-repeat");
        
        $("#payment_info").text("Du kan betale med VISA og Mastercard");
        
        $("#one_image_btn").css("background", "url('img/one_image_btn.png') no-repeat");
        $("#multiple_images_btn").css("background", "url('img/multiple_images_btn.png') no-repeat");
        $(".card_background").css("background", "url('img/backside_bg.png') no-repeat");

    }
    else {
        $("#payment_rebate_code").hide();
        
        $("#payment_info").text("Du kan betale med VISA og Mastercard");
        
        $("#one_image_btn").css("background", "url('img/one_image_btn.png') no-repeat");
        $("#multiple_images_btn").css("background", "url('img/multiple_images_btn.png') no-repeat");
        $(".card_background").css("background", "url('img/backside_bg.png') no-repeat");
    }
    
    saveInputValues($("#payment input"));
    
    if (app.user.firstName != null)
        $("#payment_first_name").val(app.user.firstName);
    
    if (app.user.lastName != null)
        $("#payment_last_name").val(app.user.lastName);
    
    if (app.user.email != null)
        $("#payment_email").val(app.user.email);
}

var paymentAgree = false;

$("#payment_agree_check").bind('touchstart', function() {
    if (paymentAgree === false) {
        $("#payment_agree_check").css("background-image", "url(img/check_on.png)");
        
        paymentAgree = true;
    }
    else {
        $("#payment_agree_check").css("background-image", "url(img/check_off.png)");
        
        paymentAgree = false;
    }
});

$(".payment_agree_btn").bind('touchstart', function() {
    showOverlay("price_terms");
});

$("#price_terms_ok_btn").bind('touchstart', function() {
    if (scrolling === false) {
        hideOverlay();
    }
});

$("#price_terms_link_btn").bind('touchstart', function() {
    if (scrolling === false) {
        functions.openExternalLink('http://www.posten.no/postens-generelle-leveringsvilkar');
    }
});

function startPayment(recurringPayment) {
    functions.showLoader("Klargjør kortbetaling");
    
    var iframeUrl = paymentUrl + "?cardId=" + app.card.cardId + "&cardPrice=" + app.card.price + "&orderId=" + app.card.orderId + "&userId=" + app.user.userId + "&deviceId=" + app.user.deviceId + "&recurringPayment=" + recurringPayment;

    $("#payment_netaxept_iframe").attr("src", iframeUrl);
}

$("#payment_ok_btn").bind('touchstart', function() {
    if ($("#payment_first_name").val() != "Ditt fornavn" && $("#payment_last_name").val() != "Ditt etternavn" && $("#payment_email").val() != "Din e-post" && paymentAgree === true) {
        console.log('Payment started - JP');
        app.card.deliveryDate = selectedDate.getFullYear() + '-' + (selectedDate.getMonth() + 1) + '-' + selectedDate.getDate();
        
        functions.uploadCard(app.card, app.user, function(card, user) {
            app.card = card;
            app.user = user;
            console.log('File upload completed - JP');

            app.storage.saveUser(app.user);
            
            functions.preparePayment(app.card, app.user, function(data) {
                app.card.orderId = data.orderId;
                
                if (data.skipPayment === true) {
                    if (lastCardNotOrderedId == app.card.cardId)
                        app.storage.removeLastCardNotOrdered();
                    
                    app.card.ordered = true;
                    app.card.orderedDate = new Date();
                    
                    app.storage.saveCard(app.card);
                    
                    setCurrentSubStep("reciept");
                    
                    showStep(app.currentSubStep, "right", true);
                    
                    functions.hideLoader();
                }
                else {
                    app.card.ordered = false;
                    app.card.orderedDate = new Date();
                    
                    app.storage.saveLastCardNotOrdered(app.card);
                    
                    lastCardNotOrderedId = app.card.cardId;
                    
                    if (app.user.hasSavedCreditCard != null) {
                        navigator.notification.confirm("Vil du benytte samme betalingskort som sist?", function (buttonIndex) {
                            var recurringPayment = 0;
                            
                            if (buttonIndex != 1)
                                recurringPayment = 1;
                            
                            startPayment(recurringPayment);

                        },"Betalingskort",["Nei", "Ja"]);
                    }
                    else {
                        startPayment(0);
                    }
                }
            });
        });
    }
    else {
        navigator.notification.alert("Informasjon mangler!", function () {}, "Betalingsinfo");
    }
});

$("#payment_first_name").bind('blur', function() {
    if ($(this).val() != "" && $(this).val() != $(this).data("defaultValue")) {
        app.user.firstName = $("#payment_first_name").val();
        
        app.storage.saveUser(app.user);
    }
});

$("#payment_last_name").bind('blur', function() {
    if ($(this).val() != "" && $(this).val() != $(this).data("defaultValue")) {
        app.user.lastName = $("#payment_last_name").val();
        
        app.storage.saveUser(app.user);
    }
});

$("#payment_email").bind('blur', function() {
    if ($(this).val() != "" && $(this).val() != $(this).data("defaultValue")) {
        app.user.email = $("#payment_email").val();
        
        app.storage.saveUser(app.user);
    }
});

$("#payment_netaxept_iframe").load(function () {
    setCurrentSubStep("payment_netaxept");
    
    showStep(app.currentSubStep, "right", true);
    
    functions.hideLoader();
});

$("#facebook_btn").bind('touchstart', function() {
    functions.openExternalLink('http://www.facebook.com/sharer.php?u=http://www.posten.no/postkort&t=Postkort%201-2-3');
});

$("#twitter_btn").bind('touchstart', function() {
    functions.openExternalLink('http://www.twitter.com/home?status=Postkort%201-2-3%20http://www.posten.no/postkort');
});

$("#send_more_btn").bind('touchstart', function() {
    app.resetApp();
});
