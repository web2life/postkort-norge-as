
function updateEditCardMenu() {
    if (app.card.filter == 0) {
        $("#bw_btn").css("background-position", "0px 0px");
    }
    else {
        $("#bw_btn").css("background-position", "0px -38px");
    }
}

var numberOfImagesAdded = 0;
var editingImage = false;

function addImage() {
    var canvas = document.getElementById("edit_image_canvas");
    
    var frameOffsetX = $("#edit_image_canvas").offset().left - $("#edit_frame").offset().left;
    var frameOffsetY = $("#edit_image_canvas").offset().top - $("#edit_frame").offset().top;
    
    var posX = canvas.width / ($("#edit_image_canvas").width() / frameOffsetX);
    var posY = canvas.height / ($("#edit_image_canvas").height() / frameOffsetY);
    
    var width = canvas.width / ($("#edit_image_canvas").width() / currentFrameWidth);
    var height = canvas.height / ($("#edit_image_canvas").height() / currentFrameHeight);
    
    var ctx = canvas.getContext("2d");
    
    var imageData = ctx.getImageData(posX * -1, posY * -1, width, height);
    
    var buffer = document.getElementById("image_canvas_" + currentImageNumber); 
    
    var bufferCtx = buffer.getContext("2d");
    
    buffer.width = width;
    buffer.height = height;
    
    bufferCtx.putImageData(imageData, 0, 0); 

    $("#image_" + currentImageNumber + " .edit_image_btn").show();
    
    if (app.card.numberOfImages === 1)
        $(".card_container").css("background", "url(img/one_image_bg2.png) no-repeat");
    
    var imageObject = new ImageObject();
    
    imageObject.imageURI = currentImageURI;
    imageObject.imageNumber = currentImageNumber;
    imageObject.zoomAmount = currentZoomAmount;
    imageObject.slider = sliderCurrentX;
    imageObject.offsetX = panningOffsetX;
    imageObject.offsetY = panningOffsetY;
    imageObject.canvasWidth = $("#image_" + currentImageNumber).width();
    imageObject.canvasHeight = $("#image_" + currentImageNumber).height();
    imageObject.posX = (parseInt($("#image_" + currentImageNumber).css("left")) - 8.5);
    imageObject.posY = (parseInt($("#image_" + currentImageNumber).css("top")) - 8);
    imageObject.editCanvasWidth = $("#edit_image_canvas").width();
    imageObject.editCanvasHeight = $("#edit_image_canvas").height();
    imageObject.frameWidth = currentFrameWidth;
    imageObject.frameHeight = currentFrameHeight;
    imageObject.frameOffsetX = frameOffsetX;
    imageObject.frameOffsetY = frameOffsetY;
    imageObject.rotateAngle = currentRotateAngle;

    app.card.images[currentImageNumber - 1] = imageObject;
    
    // Only count newly added images
    if (editingImage === false)
        numberOfImagesAdded++;
    
    if (numberOfImagesAdded == app.card.numberOfImages)
        app.currentSubStep.nextStepDisabled = false;
}

function getOrgImageSize(imageURI, callback) {
    window.resolveLocalFileSystemURI(imageURI, function(fileEntry) {
        fileEntry.file(function (fileObj) {
            var fileName = fileObj.fullPath;
            
            var orginalImage = new Image();
            
            orginalImage.src = fileName;
            
            orginalImage.onload = function() {  
                if (typeof callback == 'function')
                    callback.call(this, orginalImage.width, orginalImage.height);
            }
        });
    });
}

// Variables for keeping track of the size of the current frame
// on the image that will be created on the server
var fullFrameWidth;
var fullFrameHeight;

var maxZoomRelation;

var currentImageWidth;
var currentImageHeight;

$.getPicture = function(type, imageNumber, callback) {
    var targetSize = fullFrameWidth;
    
    if (fullFrameHeight > fullFrameWidth)
        targetSize = fullFrameHeight;
    
    //targetSize = Math.ceil(targetSize * 1.9375 / 100) * 100;
    
    targetSize = 1500;
    
    var options = {
    quality: 75,
    targetWidth: targetSize,
    targetHeight: targetSize,
    destinationType: Camera.DestinationType.FILE_URI,
    correctOrientation: true,
    };
    
    if (type == "camera") {
        options.sourceType = Camera.PictureSourceType.CAMERA;
        options.saveToPhotoAlbum = true;
    }
    else {
        options.sourceType = Camera.PictureSourceType.PHOTOLIBRARY;
    }
    
    navigator.camera.getPicture(onSuccess, onFail, options);
    
    function onSuccess(imageURI) {
        currentImageURI = imageURI;
        
        getOrgImageSize(imageURI, function(width, height) {
            currentImageWidth = width;
            currentImageHeight = height;
                        
            /*if (width < fullFrameWidth || height < fullFrameHeight) {
                navigator.notification.alert("Bildet har for lav oppløsning og kan derfor bli uklart på trykk. Vi anbefaler at du bruker et annet bilde.", function () {}, " ");                
            }*/
                        
            if (fullFrameWidth >= fullFrameHeight) {
                if (width >= height) {
                    if (width < fullFrameWidth || height < fullFrameHeight) {
                    navigator.notification.alert("Bildet har for lav oppløsning og kan derfor bli uklart på trykk. Vi anbefaler at du bruker et annet bilde.", function () {}, " ");
                    }
                }
                else {
                    if (width < fullFrameHeight || height < fullFrameWidth) {
                    navigator.notification.alert("Bildet har for lav oppløsning og kan derfor bli uklart på trykk. Vi anbefaler at du bruker et annet bilde.", function () {}, " ");
                    }
                }
            }
            else {
                if (height > width) {
                    if (width < fullFrameWidth || height < fullFrameHeight) {
                    navigator.notification.alert("Bildet har for lav oppløsning og kan derfor bli uklart på trykk. Vi anbefaler at du bruker et annet bilde.", function () {}, " ");
                    }
                }
                else {
                    if (width < fullFrameHeight || height < fullFrameWidth) {
                    navigator.notification.alert("Bildet har for lav oppløsning og kan derfor bli uklart på trykk. Vi anbefaler at du bruker et annet bilde.", function () {}, " ");
                    } 
                }
            }
                
            if (typeof callback == 'function')
                callback.call(this);
        });
    }
    
    function onFail(message) {
        //console.log("Failed because: " + message);
    }
}

function drawImageOnCanvas(imageObject, canvasId) {
    var image = new Image();
    
    image.src = currentImageURI;

    image.onload = function() {        
        var canvas = document.getElementById(this.canvasId);
        var context = canvas.getContext('2d');
        
        /*if (image.width >= image.height || (image.height > image.width && currentFrameHeight > currentFrameWidth)) {
            canvas.width = image.width;
            canvas.height = image.height;
            
            context.drawImage(image, 0, 0);
        }
        else {
            canvas.width = image.height;
            canvas.height = image.width;
            
            context.rotate(0.5 * Math.PI);
            
            canvasY = image.height * (-1);
            
            context.drawImage(image, 0, canvasY);
        }*/
        
        canvas.width = image.width;
        canvas.height = image.height;
        
        context.drawImage(image, 0, 0);
        
        var height = currentFrameHeight;
        var ratio = canvas.height / height;
        var width = canvas.width / ratio;
        
        if (width < currentFrameWidth) {
            var width = currentFrameWidth;
            var ratio = canvas.width / width;
            var height = canvas.height / ratio;
        }
        
        var left = "50%";
        var top = "50%";
        
        zoomImageOrignalWidth = width;
        zoomImageOrignalHeight = height;
        
        maxZoomRelation = currentImageWidth * 1.5 / fullFrameWidth;
        
        panningOffsetX = $("#edit_image_container").width() / 2;
        panningOffsetY = $("#edit_image_container").height() / 2;
        
        sliderStartX = 0;        
        sliderCurrentX = 0;
        
        $("#slider").css("left", (sliderCurrentX + 10) + "px");
        
        if (this.imageObject !== null) {
            left = this.imageObject.offsetX;
            top = this.imageObject.offsetY;
            
            panningOffsetX = this.imageObject.offsetX;
            panningOffsetY = this.imageObject.offsetY;
            
            sliderCurrentX = this.imageObject.slider;
            
            var width = width * this.imageObject.zoomAmount;
            var height = height * this.imageObject.zoomAmount;
            
            $("#slider").css("left", this.imageObject.slider + "px");
        }
        
        $("#" + this.canvasId).css({
            "width": width + "px",
            "height": height + "px",
            "margin-top": "-" + (height / 2) + "px",
            "margin-left": "-" + (width / 2) + "px",
            "left": left,
            "top": top,
        });
        
        if (this.imageObject !== null && this.imageObject.rotateAngle != 0) {
            rotateImage(this.imageObject.rotateAngle);
        }
    }
    
    image.canvasId = canvasId;
    image.imageObject = imageObject;
}

var zoomImageOrignalWidth;
var zoomImageOrignalHeight;
var currentZoomAmount = 1.0;
var maxZoomReached = false;

function zoomImage(zoomAmount, canvas) {
    currentZoomAmount = zoomAmount;
    
    newWidth = zoomImageOrignalWidth * zoomAmount;
    newHeight = zoomImageOrignalHeight * zoomAmount;
    
    canvas.css({
        "margin-left": "-" + newWidth / 2 + "px",
        "margin-top": "-" + newHeight / 2 + "px",
        "width": newWidth + "px",
        "height": newHeight + "px",
    });
    
    if ((newWidth / currentFrameWidth) > maxZoomRelation) {
        // Only show warning if we are zooming from min relation to max relation
        if (maxZoomReached === false) {
            navigator.notification.alert("Zoomer du nærmere enn dette kan bildet bli uskarpt på trykk", function () {}, " ");
            
            $("#slider").trigger('touchend');
            
            maxZoomReached = true;
        }
    }
    else {
        maxZoomReached = false;
    }
}

var currentRotateAngle = 0;

function rotateImage(angle) {
    currentRotateAngle += angle;
    
    if (currentRotateAngle >= 360)
        currentRotateAngle = 0;
    
    var image = new Image();
    
    image.src = currentImageURI;
    
    image.onload = function() {
        var canvas = document.getElementById("edit_image_canvas");
        var context = canvas.getContext('2d');
        
        context.save();
        
        var canvasX = 0;
        var canvasY = 0;
        
        if (image.angle == 90 || image.angle == 270) {
            canvas.width = image.height;
            canvas.height = image.width;

            context.rotate(image.angle * Math.PI / 180);

            if (image.angle == 90)
                canvasY = image.height * -1;
            else if (image.angle == 270)
                canvasX = image.width * -1;
        }
        else {
            canvas.width = image.width;
            canvas.height = image.height;
            
            context.rotate(image.angle * Math.PI / 180);
            
            if (image.angle == 180) {
                canvasX = image.width * -1;
                canvasY = image.height * -1;
            }
        }
        
        context.drawImage(image, canvasX, canvasY);
        
        // Reverse the width and height because we rotated the image
        var newWidth = $("#edit_image_canvas").height();
        var newHeight = $("#edit_image_canvas").width();
        
        // Switch start zoom width and height for correct behaviour when zooming and rotatingß
        var zoomWidth = zoomImageOrignalWidth;
        zoomImageOrignalWidth = zoomImageOrignalHeight;
        zoomImageOrignalHeight = zoomWidth;
        
        // Switch image original width and height for correct behaviour when zooming and rotating
        var imageWidth = currentImageWidth;
        currentImageWidth = currentImageHeight;
        currentImageHeight = imageWidth;
        
        // Scale the image so it fits the entire frame
        newHeight = currentFrameHeight;
        var ratio = canvas.height / newHeight;
        newWidth = canvas.width / ratio;
        
        if (newWidth < currentFrameWidth) {
            newWidth = currentFrameWidth;
            var ratio = canvas.width / newWidth;
            newHeight = canvas.height / ratio;
        }
        
        // Reset start zoom sizes
        zoomImageOrignalWidth = newWidth;
        zoomImageOrignalHeight = newHeight;
        
        maxZoomRelation = currentImageWidth / fullFrameWidth;
        
        maxZoomReached = false;
        
        newWidth = zoomImageOrignalWidth * currentZoomAmount;
        newHeight = zoomImageOrignalHeight * currentZoomAmount;
        
        context.restore();
        
        $("#edit_image_canvas").css({
            "margin-left": "-" + newWidth / 2 + "px",
            "margin-top": "-" + newHeight / 2 + "px",
            "width": newWidth + "px",
            "height": newHeight + "px",
        });
    }
    
    image.angle = currentRotateAngle;
}

$("#one_image_btn").bind('touchstart', function() {
    setCurrentSubStep("add_images");
    
    if (app.card.cardType != "one_image" || app.card.numberOfImages != 1) {
        app.card.cardType = "one_image";
        app.card.numberOfImages = 1;
        app.card.images = [];
        numberOfImagesAdded = 0;
        
        $("#add_images_card_container .card_container").html("");
        $(".card_container").css("background", "url(img/one_image_bg.png) no-repeat");
        $(".card_overlay").hide();
        
        var imageTpl = HandlebarTemplates.imageTpl({ id: 1 });
        
        $("#add_images_card_container .card_container").append(imageTpl);
        
        $("#image_1").css({
            "width": "300px",
            "height": "213px",
            "top": "8px",
            "left": "8px",
        });
        
        $("#image_1 .edit_image_btn").addClass("edit_image_large");
        
        app.currentSubStep.nextStepDisabled = true;
        
        disableNavButtons(app.currentStepIndex);
        
        resetFrameEffect();
    }
    else {
        if (app.card.images.length > 0)
            app.currentSubStep.nextStepDisabled = false;
        else
            app.currentSubStep.nextStepDisabled = true;
    }
    
    showStep(app.currentSubStep, "right", true);
});

$("#multiple_images_btn").bind('touchend', function() {
    $(".card_overlay").show();
    
    setCurrentSubStep("card_types");
    
    if (app.card.cardType === "one_image") {
        app.currentSubStep.nextStepDisabled = true;
        
        resetFrameEffect();
    }
    
    showStep(app.currentSubStep, "right", true);
});

$(".card_type_btn").bind('touchend', function() {
    if (!scrolling) {
        app.card.cardType = $(this).attr("id");
        
        $("#number_of_images .vertical_center").html("");
        
        for (var i = 2; i <= 5; i++) {
            var context = { id: i };
            
            var numberOfImagesTpl = HandlebarTemplates.numberOfImagesTpl(context);
            
            $("#number_of_images .vertical_center").append(numberOfImagesTpl);
            
            $("#number_of_images").find("#number_of_images_" + i).css("background-image", "url(img/number_of_images/" + app.card.cardType + "_" + i + ".png)");
        }
        
        app.currentSubStep.nextStepDisabled = false;
        
        setCurrentSubStep("number_of_images");
        
        app.currentSubStep.nextStepDisabled = true;
        
        disableNavButtons(app.currentStepIndex);
        
        showStep(app.currentSubStep, "right", true);
    }
});

$("#number_of_images").on('touchend', ".number_of_images_btn", function() {
    app.card.images = [];
    
    app.card.numberOfImages = $(this).attr("id").split("_")[3];
    
    $(".card_container").css("background", "url(img/card_types/" + app.card.cardType + "_" + app.card.numberOfImages + ".png) no-repeat");
    
    $(".card_overlay").css("background", "url(img/card_types_overlay/" + app.card.cardType + "_" + app.card.numberOfImages + ".png) no-repeat");
    
    $("#add_images_card_container .card_container").html("");
    
    for (var i = 1; i <= app.card.numberOfImages; i++) {
        var context = { id: i };
        
        var imageTpl = HandlebarTemplates.imageTpl(context);
        
        $("#add_images_card_container .card_container").append(imageTpl);
        
        var image = cardTypes[app.card.cardType]["images" + app.card.numberOfImages]["image" + i];
        
        $("#image_" + i).css({
            "width": image.width + "px",
            "height": image.height + "px",
            "top": image.top + "px",
            "left": image.left + "px",        });
        
        $("#image_" + i + " .edit_image_btn").addClass("edit_image_" + image.size);
        
        if (image.rotate !== 0) {
            //$("#image_" + i+ " canvas").css("-webkit-transform", "rotate(" + image.rotate + "deg)");
        }
    }
    
    app.currentSubStep.nextStepDisabled = false;
    
    setCurrentSubStep("add_images");
    
    app.currentSubStep.nextStepDisabled = true;
    numberOfImagesAdded = 0;
    
    showStep(app.currentSubStep, "right", true);
});

$("#add_images_card_container").on('touchstart', ".image_canvas", function() {
    var imageWidth = $(this).width();
    var imageHeight = $(this).height();
    
    if (imageWidth >= imageHeight) {
        frameWidth = 230;
        frameHeight = Math.round(frameWidth / (imageWidth / imageHeight));
    }
    else {
        frameHeight = 230;
        frameWidth = Math.round(frameHeight / (imageHeight / imageWidth));
    }
    
    $("#edit_frame").css({
        "width": frameWidth + "px",
        "height": frameHeight + "px",
        "margin-top": "-" + (frameHeight / 2) + "px",
        "margin-left": "-" + (frameWidth / 2) + "px",
    });
    
    currentImageNumber = parseInt($(this).attr("id").split("_")[2]);
    
    currentFrameWidth = frameWidth;
    currentFrameHeight = frameHeight;
    
    currentZoomAmount = 1.0;
    currentRotateAngle = 0;
    
    if ($("#image_" + currentImageNumber + " .edit_image_btn").is(":visible"))
        $("#edit_image_btn").show();
    else
        $("#edit_image_btn").hide();
    
    
    fullFrameWidth = Math.round((1200 / (300 / imageWidth)));
    fullFrameHeight = Math.round((851 / (213 / imageHeight)));
    
    maxZoomReached = false;
    
    showOverlay("add_edit_image");
});

$("#camera_btn").bind('touchstart', function() {
    $.getPicture("camera", currentImageNumber, function() {
        editingImage = false;
        
        $("#edit_image_hand").show();
        
        drawImageOnCanvas(null, "edit_image_canvas");
        
        setCurrentSubStep("edit_images");
        
        showStep(app.currentSubStep, "right", true);
    });
});

$("#library_btn").bind('touchstart', function() {
    $.getPicture("library", currentImageNumber, function() {
        editingImage = false;
        
        $("#edit_image_hand").show();
        
        drawImageOnCanvas(null, "edit_image_canvas");        
        
        setCurrentSubStep("edit_images");
        
        showStep(app.currentSubStep, "right", true);
    });
});

$("#edit_image_btn").bind('touchstart', function() {
    editingImage = true;
    
    $("#edit_image_hand").hide();
    
    currentImageURI = app.card.images[currentImageNumber - 1].imageURI;
    currentZoomAmount = app.card.images[currentImageNumber - 1].zoomAmount;
    currentFrameWidth = app.card.images[currentImageNumber - 1].frameWidth;
    currentFrameHeight = app.card.images[currentImageNumber - 1].frameHeight;
    currentRotateAngle = 0;
                          
    // Reset the edit image canvas
    var canvas = document.getElementById("edit_image_canvas");
    canvas.width = canvas.width;
    
    drawImageOnCanvas(app.card.images[currentImageNumber - 1], "edit_image_canvas");
    
    setCurrentSubStep("edit_images");
    
    showStep(app.currentSubStep, "right", true);
});

$(".cancel_btn").bind('touchstart', function() {
    functions.hideKeyboard();
                      
    hideOverlay();
});

$(".back_btn").bind('touchstart', function() {
    functions.hideKeyboard();
                    
    hideOverlay();
});

var sliderStartX = 0;
var sliderCurrentX = 0;
var sliderNewX = 0;

$("#slider").bind('touchstart', function(event) {
    sliderStartX = event.originalEvent.touches[0].pageX;
}).bind('touchmove', function(event) {
    var sliderTouchX = event.originalEvent.touches[0].pageX;
    var diffX = sliderTouchX - sliderStartX;
    sliderNewX = sliderCurrentX + diffX;
    
    if (sliderNewX <= 0)
        sliderNewX = 0;
    else if (sliderNewX >= 150)
        sliderNewX = 150;
    
    $(this).css("left", (sliderNewX + 10) + "px");
    
    sliderValue = sliderNewX / 160 + 1.0;
    
    zoomImage(sliderValue, $("#edit_image_canvas"));
}).bind('touchend', function(event) {
    sliderCurrentX = $(this).position().left;
});

$("#rotate_btn").bind('touchstart', function() {
    rotateImage(90);
});

$("#center_btn").bind('touchstart', function() {
    panningOffsetX = $("#edit_image_container").width() / 2;
    panningOffsetY = $("#edit_image_container").height() / 2;
    
    $("#edit_image_canvas").css({
        "top": "50%",
        "left": "50%",
    });
});

var panningStartX;
var panningStartY;

var panningEndX;
var panningEndY;

var panningOffsetX = 0;
var panningOffsetY = 200;

var newPanningOffsetX = 0;
var newPanningOffsetY = 0;

$("#edit_image_canvas").bind('touchstart', function(event) {
    $("#edit_image_hand").hide();
    
    panningStartX = event.originalEvent.touches[0].pageX;
    panningStartY = event.originalEvent.touches[0].pageY;
}).bind('touchmove', function(event) {
    panningEndX = event.originalEvent.touches[0].pageX;
    panningEndY = event.originalEvent.touches[0].pageY;
    
    diffX = panningEndX - panningStartX;
    diffY = panningEndY - panningStartY;
    
    newPanningOffsetX = panningOffsetX + diffX;
    newPanningOffsetY = panningOffsetY + diffY;
    
    $(this).css({
        "left": newPanningOffsetX + "px",
        "top": newPanningOffsetY + "px",
    });
}).bind('touchend', function(event) {
    panningOffsetX = newPanningOffsetX;
    panningOffsetY = newPanningOffsetY;
});


$("#bw_btn").bind('touchstart', function() {
    functions.showLoader("Oppdaterer bilde");
    
    var canvas =  $("#edit_card_container canvas");

    var imagesDone = 0;
    
    canvas.each(function (i) {
        var canvasId = $(this).attr("id");
        
        if (app.card.filter == 0) {
            functions.saveColorCanvas(canvasId, i);
            
            Caman("#" + canvasId, function() {            
                this.greyscale().render(function() {
                    imagesDone++;
                    
                    if (imagesDone == app.card.numberOfImages) {
                        app.card.filter = 1;
                        
                        updateEditCardMenu();
                        
                        functions.hideLoader();
                    }     
                });            
            });
        }
        else {
            functions.resetColorCanvas(canvasId, i);
            
            imagesDone++;
            
            if (imagesDone == app.card.numberOfImages) {
                app.card.filter = 0;
                
                updateEditCardMenu();
                
                functions.hideLoader();
            }
        }
    });
});

$("#rotate_effects_btn").bind('touchstart', function() {
    if (app.card.frameRotated == 0) {
        app.card.frameRotated = 1;
        
        $(".card_frame").css("background", "url(img/frames/frame_rotated_" + app.card.frameId + ".png) no-repeat");
    }
    else {
        app.card.frameRotated = 0;
        
        $(".card_frame").css("background", "url(img/frames/frame_" + app.card.frameId + ".png) no-repeat");
    }
});

$("#preview_btn").bind('touchstart', function() {
    cloneCard("edit_card_container", "edit_preview_container", "edit_preview_image_");
    
    showOverlay("edit_preview");
    
    var left = $("#edit_preview_container").position().top + $("#edit_preview_container").outerHeight(true) - 9;
    var top = $("#edit_preview_container").position().left + $("#edit_preview_container").outerWidth(true) - 9;
    
    $("#edit_preview_close_btn").css({
    	"top":  (top * 1.3 - 4) + "px",
    	"left": (left * 1.3 - 36) + "px",
    });
});

$("#edit_preview_close_btn").bind('touchstart', function() {
    hideOverlay();
});

function resetFrameEffect() {
    app.card.frameId = 0;
    
    app.card.frameRotated = 0;
    
    $(".card_frame").css("background-image", "url(img/frames/frame_0.png)");
}

$(".frame_btn").bind('touchend', function() {
    if (scrolling === false) {
        var frameId = $(this).attr("id").split("_")[1];
        
        app.card.frameId = frameId;
        
        if (app.card.frameRotated == 1)
            $(".card_frame").css("background-image", "url(img/frames/frame_rotated_" + frameId + ".png)");
        else
            $(".card_frame").css("background-image", "url(img/frames/frame_" + frameId + ".png)");
    }
});

