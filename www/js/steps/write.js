var editTextMenuShowing = false;

function hideEditTextMenu() {
    $("#edit_text_menu_btn").css("background-position", "0px 0px").animate({ "bottom": "0px"});
    
    $("#edit_text_menu").slideUp("normal", function() {
        editTextMenuShowing = false;
        
        $("#text").removeAttr("readonly");
    });
}

function showEditTextMenu() {
    $("#edit_text_menu_btn").css("background-position", "0px -22px").animate({ "bottom": "170px"});
    
    $("#edit_text_menu").slideDown("normal", function() {
        editTextMenuShowing = true;
    });
}

$("#edit_text_menu").bind('touchend', function(event) {});

$("#edit_text_menu_btn").bind('touchend', function() {
    $("#text").attr("readonly", "readonly");
    
    if (editTextMenuShowing == false) {
        showEditTextMenu();
    }
    else {
        hideEditTextMenu();
    }
});

$("#text").bind('touchstart', function(e) {
    $(this).removeAttr("readonly");
    
    hideEditTextMenu();
});

$("#text").bind('blur', function() {
    $(this).attr("readonly", "readonly");
    
    if (editTextMenuShowing == false)
        showEditTextMenu();
    
    updateText($(this).val());
});

$(".font_btn").bind('touchstart', function() {
    var self = this;
    
    var fontId = $(this).attr("id").split("_")[1];
    
    changeFont(fontId, function() {
        $("#edit_text_font img").each(function() {
            $(this).attr("src", "img/fonts/font_" + $(this).attr("id").split("_")[1] + ".png");
        });
        
        $(self).attr("src", "img/fonts/font_" + fontId + "b.png");
    });
});

$(".color_btn").bind('touchstart', function() {
    var colorId = $(this).attr("id").split("_")[1];
    
    changeFontColor(colorId);
    
    $("#edit_text_color img").each(function(index) {
        var currentColorId = $(this).attr("id").split("_")[1];
            
        $(this).attr("src", "img/colors/color_" + currentColorId + ".png");
    });
        
    $(this).attr("src", "img/colors/color_" + colorId + "b.png");
});

$(".change_text_size_btn").bind('touchstart', function() {
    updateFontSize($(this).attr("id"));
});

$(".change_text_align_btn").bind('touchstart', function() {
    changeTextAlign($(this).attr("id").split("_")[2]);
});

$("#write_address_btn").bind('touchend', function() {
    showOverlay("add_address");
});

$("#search_address_btn").bind('touchend', function() {
    $("#search_phone").data("defaultValue", $("#search_phone").val());
    
    showOverlay("search_address");
});

function chooseContact() {
    var options = {
    allowsEditing: "false"
    };
    
    navigator.contacts.chooseContact(chooseContactSuccess, options);
}

var _contactName;

function chooseContactSuccess(id) {
    _contactName = "";
    
    var contactOptions = new ContactFindOptions();
    contactOptions.filter = "";
    contactOptions.multiple = true;
    
    var fields = ['*'];
    
    navigator.contacts.find(fields, onPickContactSuccess, onPickContactError, contactOptions);
    
    function onPickContactSuccess(contacts) {
        for (var i = 0; i < contacts.length; i++) {
            if (contacts[i].id == id) {
                if (contacts[i].addresses != null) {
                    if (contacts[i].addresses.length > 0) {
                        if (contacts[i].addresses.length > 1) {
                            _contactName = contacts[i].name.formatted;
                            
                            showSelectAddresses(contacts[i].addresses);
                            
                            break;
                        }
                        else {
                            if (contacts[i].addresses[0].streetAddress != null &&contacts[i].addresses[0].postalCode != null && contacts[i].addresses[0].locality != null) {
                                
                                var address = {
                                    name: contacts[i].name.formatted,
                                    street: contacts[i].addresses[0].streetAddress,
                                    postCode: contacts[i].addresses[0].postalCode + " " + contacts[i].addresses[0].locality,
                                }
                                
                                addAddress(address);
                                
                                hideOverlay();
                                
                                break;
                            }
                            else {
                                navigator.notification.alert("Denne kontakten er ikke lagret med fullstendig adresse eller telefonnummer.", function () {}, "Mangler informasjon");
                                
                                break;
                            }
                        }
                    }
                }
                else if (contacts[i].phoneNumbers != null) {
                    if (contacts[i].phoneNumbers.length > 1) {
                        showSelectPhoneNumbers(contacts[i].phoneNumbers);
                        
                        break;
                    }
                    else {
                        $("#search_phone").val(contacts[i].phoneNumbers[0].value);
                        
                        break;
                    }
                }
                else {
                    navigator.notification.alert("Denne kontakten er ikke lagret med fullstendig adresse eller telefonnummer.", function () {}, "Mangler informasjon");
                    
                    break;
                }
            }
        }
    }
    
    function onPickContactError(contactError) {
        navigator.notification.alert("Denne kontakten er ikke lagret med fullstendig adresse eller telefonnummer.", function () {}, "Mangler informasjon");
    }
}

var _phoneNumbers;

function onPhoneNumberSelected(button) {
    resetInputValues($("#search_address input"));
    
    var index = button - 1;
    
    if (index < _phoneNumbers.length)
        $("#search_phone").val(_phoneNumbers[index].value);
}

var _addresses;

function onAddressSelected(button) {
    resetInputValues($("#search_address input"));
    
    var index = button - 1;
    
    if (index < _addresses.length) {
        var address = {
            name: _contactName,
            street: _addresses[index].streetAddress,
            postCode: _addresses[index].postalCode + " " + _addresses[index].locality,
        }
        
        addAddress(address);
        
        hideOverlay();
    }
}

function showSelectPhoneNumbers(phoneNumbers) {
    var buttonLabels = [];
    
    _phoneNumbers = phoneNumbers;
    
    for (var i = 0; i < phoneNumbers.length; i++) {
        buttonLabels[i] = translatePhoneNumberType(phoneNumbers[i].type) + ": " + phoneNumbers[i].value;
    }
    
    buttonLabels.push("Avbryte");
    
    navigator.notification.confirm("",
                                   onPhoneNumberSelected,
                                   "Velg telefonnummer",
                                   buttonLabels
                                   );
}

function showSelectAddresses(addresses) {
    var buttonLabels = [];
    
    _addresses = addresses;
    
    for (var i = 0; i < addresses.length; i++) {
        if (addresses[i].streetAddress != null && addresses[i].postalCode != null && addresses[i].locality != null) {
            buttonLabels[i] = translatePhoneNumberType(addresses[i].type) + ": " + addresses[i].streetAddress + ", " + addresses[i].postalCode + " " + addresses[i].locality;
        }
    }
    
    buttonLabels.push("Avbryte");
    
    navigator.notification.confirm("",
                                   onAddressSelected,
                                   "Velg mottakere",
                                   buttonLabels
                                   );
}

function translatePhoneNumberType(type) {
    switch (type) {
        case "mobile":
            return "mobil";
            break;
        case "home":
            return "hjem";
            break;
        case "work":
            return "arbeid";
            break;
        default:
            return type;
            break;
    }
}


$("#search_contacts_btn").bind('touchend', function() {
    chooseContact();
});

function addAddress(address) {
    app.card.address[app.card.address.length] = address;
    
    updateAddressInfo();
}

function deleteAddress(index) {
    app.card.address.splice(index, 1);
    
    swipeDeleteActive = 0;
    
    updateAddressInfo();
}

function updateAddressInfo() {
    var infoText = "INGEN MOTTAKERE";
    
    if (app.card.address.length > 0) {
        infoText = app.card.address.length + " MOTTAKERE";
    }
    
    $(".address_info_number").text(infoText);
    
    if (app.card.address.length > 0) {
        app.steps[2].subSteps[1].nextStepDisabled = false;
        app.steps[2].subSteps[2].nextStepDisabled = false;
        
        navRightBtnDisabled = false;
    }
    else {
        app.steps[2].subSteps[1].nextStepDisabled = true;
        app.steps[2].subSteps[2].nextStepDisabled = true;
        
        navRightBtnDisabled = true;
        
        if (app.currentStepIndex == 2)
            disableNavButtons(2);
    }
    
    $("#address_list ul").html("");
    
    for (var i = 0; i < app.card.address.length; i++) {
        var address = app.card.address[i];
        
        var item = HandlebarTemplates.addressListItemTpl(address);
        
        $("#address_list ul").append(item);
        
        $("#address_list").find("li").addClass("edit_address_item");
        $("#address_list").find(".list_arrow").remove();
    }
    
    updateMenu();
}

$("#add_address_btn").bind('touchstart', function() {
    if ($("#add_name_input").data("defaultValue") &&
        $("#add_name_input").val() != $("#add_name_input").data("defaultValue") &&
        $("#add_street_input").data("defaultValue") &&
        $("#add_street_input").val() != $("#add_address_input").data("defaultValue") &&
        $("#add_name_input").data("defaultValue") &&
        $("#add_post_code_input").val() != $("#add_post_code_input").data("defaultValue")) {
        
        var address = {
            name: $("#add_name_input").val(),
            street: $("#add_street_input").val(),
            postCode: $("#add_post_code_input").val(),
        };
        
        addAddress(address);
    }
    
    hideOverlay();
});

var editAddressIndex = 0;

$("#edit_address_btn").bind('touchstart', function() {
    var address = {
    name: $("#edit_name_input").val(),
    street: $("#edit_street_input").val(),
    postCode: $("#edit_post_code_input").val(),
    };
    
    $("#address_list li").eq(editAddressIndex).find(".name").text(address.name);
    $("#address_list li").eq(editAddressIndex).find(".address").text(address.street + ", " + address.postCode);
    
    app.card.address[editAddressIndex] = address;
    
    hideOverlay();
});

$("#delete_address_btn").bind('touchstart', function() {
    deleteAddress(editAddressIndex);
    
    hideOverlay();
});

var searchResult;

$("#search_btn").bind('touchend', function() {
    $("#search_address_result_list ul li").remove();
    
    var firstName = $("#search_first_name").val();
    var lastName = $("#search_last_name").val();
    var city = $("#search_city").val();
    var phone = $("#search_phone").val();
    
    functions.searchAddress(firstName, lastName, city, phone, function(result, items) {
        searchResult = result;
        
        $("#search_address_result_list ul").append(items);
        
        functions.hideLoader();
        
        showOverlay("search_address_result_list");
    });
});

$("#search_address_result_list").on('touchend', ".add_address_item", function() {
    if (scrolling === false) {
        addAddress(searchResult[$(this).index()]);
        
        resetInputValues($("#search_address input"));
        
        hideOverlay();
    }
});

function turnPreviewCard() {
    if ($(".card").hasClass("flipped")) {
        $(".card").removeClass("flipped");
    }
    else {
        $(".card").addClass("flipped");
    }
}

$(".flip_card_btn").bind('touchstart', function(e) {
    e.stopPropagation();
    turnPreviewCard();
});

var maxTime = 1000;
var maxDistance = 50;
var startTime = 0;
var startX = 0;
var swipeDeleteActive = 0;

$("#address_list").on('touchend', ".edit_address_item", function() {
    if (scrolling === false && swipeDeleteActive == 0) {
        editAddressIndex = $(this).index();
        
        var address = app.card.address[editAddressIndex];
        
        saveInputValues($("#edit_address input"));
        
        $("#edit_name_input").val(address.name);
        $("#edit_street_input").val(address.street);
        $("#edit_post_code_input").val(address.postCode);

        showOverlay("edit_address");
    }
    
    startTime = 0;
    startX = 0;
});

$("#address_list").on('touchstart', ".edit_address_item", function(event) {
    startTime = event.timeStamp;
    startX =  event.originalEvent.touches[0].pageX;
})

$("#address_list").on('touchmove', ".edit_address_item", function(event) {
    var self = $(this);
    var currentX = event.originalEvent.touches[0].pageX;
    var currentDistance = (startX === 0) ? 0 : Math.abs(currentX - startX),
    
    currentTime = event.timeStamp;
    
    if (startTime !== 0 && currentTime - startTime < maxTime && currentDistance > maxDistance) {
        if (currentX < startX) {
            if (swipeDeleteActive == 0 || self.find('.delete_address_btn').length == 0) {
                $('.delete_address_btn').remove();
                
                swipeDeleteActive = 1;
                
                var $deleteBtn = $("<div />").addClass("delete_address_btn");
                
                self.prepend($deleteBtn);
            }
            else {
                $('.delete_address_btn').remove();
                
                swipeDeleteActive = 0;
            }
        }
        
        startTime = 0;
        startX = 0;
    }
});

$("#address_list").on('touchstart', ".delete_address_btn", function(event) {
    deleteAddress($(this).parent().index());
});
