var cardTypes = {
    "simple": {
        "images2": {
            "image1": {
                width: 138,
                height: 197,
                top: 16,
                left: 16.5,
                rotate: 0,
                size: "large",
            },
            
            "image2": {
                width: 138,
                height: 197,
                top: 16,
                left: 163,
                rotate: 0,
                size: "large",
            },
        },
        
        "images3": {
            "image1": {
                width: 138,
                height: 197,
                top: 16,
                left: 16.5,
                rotate: 0,
                size: "large",
            },
            
            "image2": {
                width: 138,
                height: 94.5,
                top: 16,
                left: 162.5,
                rotate: 0,
                size: "large",
            },
            
            "image3": {
                width: 138,
                height: 94.5,
                top: 118.5,
                left: 162.5,
                rotate: 0,
                size: "large",
            },
        },
        
        "images4": {
            "image1": {
                width: 138,
                height: 94.5,
                top: 16,
                left: 16.5,
                rotate: 0,
                size: "large",
            },
            
            "image2": {
                width: 138,
                height: 94.5,
                top: 16,
                left: 162.5,
                rotate: 0,
                size: "large",
            },
            
            "image3": {
                width: 138,
                height: 94.5,
                top: 118.5,
                left: 16.5,
                rotate: 0,
                size: "large",
            },
            
            "image4": {
                width: 138,
                height: 94.5,
                top: 118.5,
                left: 162.5,
                rotate: 0,
                size: "large",
            },
        },
        
        "images5": {
            "image1": {
                width: 186.5,
                height: 128,
                top: 16,
                left: 16.5,
                rotate: 0,
                size: "large",
            },
            
            "image2": {
                width: 89.5,
                height: 128,
                top: 16,
                left: 211,
                rotate: 0,
                size: "large",
            },
            
            "image3": {
                width: 89.5,
                height: 60.5,
                top: 152,
                left: 16,
                rotate: 0,
                size: "small",
            },
            
            "image4": {
                width: 89.5,
                height: 60.5,
                top: 152,
                left: 114,
                rotate: 0,
                size: "small",
            },
            
            "image5": {
                width: 89.5,
                height: 60.5,
                top: 152,
                left: 211,
                rotate: 0,
                size: "small",
            },
        },
    },
    
    "rectangle": {
        "images2": {
            "image1": {
                width: 138,
                height: 138,
                top: 45.5,
                left: 16.5,
                rotate: 0,
                size: "large",
            },
            
            "image2": {
                width: 138,
                height: 138,
                top: 45.5,
                left: 163,
                rotate: 0,
                size: "large",
            },
        },
        
        "images3": {
            "image1": {
                width: 187,
                height: 187,
                top: 16,
                left: 16.5,
                rotate: 0,
                size: "large",
            },
            
            "image2": {
                width: 89.5,
                height: 89.5,
                top: 16,
                left: 211,
                rotate: 0,
                size: "small",
            },
            
            "image3": {
                width: 89.5,
                height: 89.5,
                top: 113.5,
                left: 211,
                rotate: 0,
                size: "small",
            },
        },
        
        "images4": {
            "image1": {
                width: 94.5,
                height: 94.5,
                top: 16,
                left: 16.5,
                rotate: 0,
                size: "small",
            },
            
            "image2": {
                width: 94.5,
                height: 94.5,
                top: 16,
                left: 119,
                rotate: 0,
                size: "small",
            },
            
            "image3": {
                width: 94.5,
                height: 94.5,
                top: 118.5,
                left: 16.5,
                rotate: 0,
                size: "small",
            },
            
            "image4": {
                width: 94.5,
                height: 94.5,
                top: 118.5,
                left: 119,
                rotate: 0,
                size: "small",
            },
        },
        
        "images5": {
            "image1": {
                width: 115,
                height: 115,
                top: 16,
                left: 39.5,
                rotate: 0,
                size: "large",
            },
            
            "image2": {
                width: 115,
                height: 115,
                top: 16,
                left: 162.5,
                rotate: 0,
                size: "large",
            },
            
            "image3": {
                width: 74,
                height: 74,
                top: 139,
                left: 39.5,
                rotate: 0,
                size: "small",
            },
            
            "image4": {
                width: 74,
                height: 74,
                top: 139,
                left: 121.5,
                rotate: 0,
                size: "small",
            },
            
            "image5": {
                width: 74,
                height: 74,
                top: 139,
                left: 203.5,
                rotate: 0,
                size: "small",
            },
        },
    },
    
    "rugged": {
        "images2": {
            "image1": {
            width: 138,
            height: 197,
            top: 16,
            left: 16.5,
            rotate: 0,
            size: "large",
            },
            
            "image2": {
            width: 138,
            height: 197,
            top: 16,
            left: 163,
            rotate: 0,
            size: "large",
            },
        },
        
        "images3": {
            "image1": {
            width: 138,
            height: 197,
            top: 16,
            left: 16.5,
            rotate: 0,
            size: "large",
            },
            
            "image2": {
            width: 138,
            height: 94.5,
            top: 16,
            left: 162.5,
            rotate: 0,
            size: "large",
            },
            
            "image3": {
            width: 138,
            height: 94.5,
            top: 118.5,
            left: 162.5,
            rotate: 0,
            size: "large",
            },
        },
        
        "images4": {
            "image1": {
            width: 138,
            height: 94.5,
            top: 16,
            left: 16.5,
            rotate: 0,
            size: "large",
            },
            
            "image2": {
            width: 138,
            height: 94.5,
            top: 16,
            left: 162.5,
            rotate: 0,
            size: "large",
            },
            
            "image3": {
            width: 138,
            height: 94.5,
            top: 118.5,
            left: 16.5,
            rotate: 0,
            size: "large",
            },
            
            "image4": {
            width: 138,
            height: 94.5,
            top: 118.5,
            left: 162.5,
            rotate: 0,
            size: "large",
            },
        },
        
        "images5": {
            "image1": {
            width: 186.5,
            height: 128,
            top: 16,
            left: 16.5,
            rotate: 0,
            size: "large",
            },
            
            "image2": {
            width: 89.5,
            height: 128,
            top: 16,
            left: 211,
            rotate: 0,
            size: "large",
            },
            
            "image3": {
            width: 89.5,
            height: 60.5,
            top: 152,
            left: 16,
            rotate: 0,
            size: "small",
            },
            
            "image4": {
            width: 89.5,
            height: 60.5,
            top: 152,
            left: 114,
            rotate: 0,
            size: "small",
            },
            
            "image5": {
            width: 89.5,
            height: 60.5,
            top: 152,
            left: 211,
            rotate: 0,
            size: "small",
            },
        },
    },
    
    "random": {
        "images2": {
            "image1": {
                width: 128,
                height: 178,
                top: 29,
                left: 26,
                rotate: 5.25,
                size: "large",
            },
            
            "image2": {
                width: 128,
                height: 178,
                top: 18,
                left: 166,
                rotate: 3.25,
                size: "large",
            },
        },
        
        "images3": {
            "image1": {
                width: 129,
                height: 182,
                top: 23,
                left: 26,
                rotate: -5.1,
                size: "large",
            },
            
            "image2": {
                width: 120,
                height: 86,
                top: 20,
                left: 166,
                rotate: 3.5,
                size: "large",
            },
            
            "image3": {
                width: 120,
                height: 87,
                top: 121,
                left: 175,
                rotate: -3.5,
                size: "large",
            },
        },
        
        "images4": {
            "image1": {
                width: 118,
                height: 84,
                top: 18,
                left: 40,
                rotate: 2,
                size: "large",
            },
            
            "image2": {
                width: 120,
                height: 86,
                top: 25,
                left: 172,
                rotate: -5.5,
                size: "large",
            },
            
            "image3": {
                width: 120,
                height: 86,
                top: 121,
                left: 23,
                rotate: -5.5,
                size: "large",
            },
            
            "image4": {
                width: 118,
                height: 86,
                top: 123,
                left: 156,
                rotate: -3.5,
                size: "large",
            },
        },
        
        "images5": {
            "image1": {
                width: 128,
                height: 92,
                top: 29,
                left: 20,
                rotate: 5.5,
                size: "large",
            },
            
            "image2": {
                width: 128,
                height: 92,
                top: 22,
                left: 167,
                rotate: -5.5,
                size: "large",
            },
            
            "image3": {
                width: 82,
                height: 60,
                top: 141,
                left: 19,
                rotate: 3.25,
                size: "small",
            },
            
            "image4": {
                width: 82,
                height: 60,
                top: 136,
                left: 116,
                rotate: -3,
                size: "small",
            },
            
            "image5": {
                width: 82,
                height: 60,
                top: 140,
                left: 213,
                rotate: 3.25,
                size: "small",
            },
        },
    },
}