
var navLeftBtnDisabled = false;
var navRightBtnDisabled = false;

var currentImageNumber = 1;

var currentImageURI;
var currentFrameWidth;
var currentFrameHeight;

var app = {
    initialize: function() {
        $("#info_box").hide();

        this.bindEvents();
        
        this.firstCard = true;
        
        this.currentOverlay = "start";
        this.currentOverlayShowing = true;
        
        this.storage = new Storage();
        
        
        var firstrun = window.localStorage.getItem("runned222");
        if ( firstrun == null ) {
            window.localStorage.setItem("runned", "1");
             $("#info_box").show();
        
        }
        
      
        
        this.steps = [new Step("home", new Array(new SubStep("home", false, null, "start"))),
                     
                     new Step("card", new Array(
                                                new SubStep("card", true, null, null, "Har du ett eller flere bilder?"),
                                                new SubStep("card_types", true, "number_of_images", null, "Velg stil på collagen din"),
                                                new SubStep("number_of_images", true, null, null, "Hvor mange bilder har du?"),
                                                new SubStep("add_images", true, "edit_card", "number_of_images", "Legg til bilder"),
                                                new SubStep("edit_images", false, "add_images", null, "Tilpass bildet til rammen"),
                                                new SubStep("edit_card", false, "write_text", "add_images", "Legg til effekter"))),
                     
                     new Step("write", new Array(
                                                 new SubStep("write_text", false),
                                                 new SubStep("write_address", true, "preview"),
                                                 new SubStep("preview", true, "delivery_date", null, "Forhåndsvisning"))),
                     
                     new Step("send", new Array(
                                                new SubStep("delivery_date", false, "preview_send", null, "Velg leveringsdag"),
                                                new SubStep("preview_send", false),
                                                new SubStep("payment", true),
                                                new SubStep("payment_netaxept", true),
                                                new SubStep("reciept", true, null, null, null, true))),
                      ];
    },
    
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
        
        functions.checkInternetConnection();
        
        deviceReady();
    },
    
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        //console.log('Received Event: ' + id);
    },
        
    slideStep: function(step, stepDest) {
        var currentStepDest;
        var self = this;
        
        // If there is no current page (app just started) -> No transition: Position new page in the view port
        if (!this.currentStepName) {
            $("#" + step).attr("class", "step stage-center");
            
            $("#" + step).show();
            
            this.currentStepName = step;
            
            return;
        }
        
        $(".stage-right, .stage-left").hide();
        
        if (stepDest === "left") {
            // Backward transition (slide from left)
            $("#" + step).attr("class", "step stage-left");
            currentStepDest = "stage-right";
        }
        else {
            // Forward transition (slide from right)
            $("#" + step).attr("class", "step stage-right");
            currentStepDest = "stage-left";
        }
        
        $("#" + this.currentStepName).css("position", "absolute")
        $("#" + step).css("position", "absolute").show();
        
        // Wait until the new page has been added to the DOM...
        setTimeout(function() {
            // Slide out the current page
            $("#" + self.currentStepName).attr('class', 'step transition ' + currentStepDest);
            
            // Slide in the new page
            $("#" + step).attr('class', 'step stage-center transition');
            
            self.currentStepName = step;
        });
        
    },
        
    slideOverlay: function(overlay, show) {
        var self = this;
        
        $("#" + overlay).show();
        
        if (show === true) {
            overlayDest = "stage-top";
        }
        else {
            overlayDest = "stage-bottom";
        }
        
        // Wait until the new page has been added to the DOM...
        setTimeout(function() {
            // Slide out the current page
            $("#" + overlay).attr('class', 'overlay transition ' + overlayDest);
            
            self.currentOverlay = overlay;
            self.currentOverlayShowing = show;
        });
    },
        
    verticalCenter: function(parentId) {
        $element = $("#" + parentId + " .vertical_center");
        
        $element.css("margin-top", "-" + $element.height() / 2 + "px");
    },
    
    messageReceived: function(message) {
        var self = this;
        
        if (message.data == "paymentFinished") {                        
            app.user.hasSavedCreditCard = true;
            
            app.storage.saveUser(app.user);
            
            functions.sendCard(app.card, app.user, function() {
                if (lastCardNotOrderedId === app.card.cardId)
                    app.storage.removeLastCardNotOrdered();
                
                app.card.ordered = true;
                app.card.orderedDate = new Date();
                               
                app.storage.saveCard(app.card);
                               
                setCurrentSubStep("reciept");
                
                showStep(app.currentSubStep, "right", true);
            });
        }
        else if (message.data == "paymentFailed") {            
            navigator.notification.alert("Betaling feilet! Vennligst prøv på nytt.", function () {}, "Betalingsinfo");
            
            setCurrentSubStep("payment");
            
            showStep(app.currentSubStep, "left", true);
        }
        else {
            functions.serverNotResponding();
        }
    },
    
    resetApp: function() {
        deviceReady();
    },
};

window.addEventListener("message", app.messageReceived, false);

$("body").on('webkitTransitionEnd', ".step", function () {
    if ($(this).attr("id") != app.currentStepName)
        $(this).hide();
    else if ($(this).attr("id") == "write_text")
        setMaximumTextHeight(app.card.fontId, app.card.fontSizeId, app.card.fontSizePx);
});

$("body").on('webkitTransitionEnd', ".overlay", function (){
    $(".overlay").attr("class", "overlay stage-bottom").hide();
    
    if (app.currentOverlayShowing)
        $(this).attr("class", "overlay stage-top").show();
});

// Reanable the inputs if the are disabled on touchstart (to disable them during a touchstart se comment below)
$("body").bind('touchstart', function() {
    if (disableInputs) {
        disableInputs = false;
    }
});

// To prevent inputs to recieve focus through overlaying buttons, set disableInputs to true on the action for the button and remeber to call stopPropagation on the event
$("body").on('focus', 'input', function(e) {
    if (disableInputs) {
        e.stopPropagation();
        $(this).blur();
    }
    else {
        functions.keyboardShowing = true;
    }
});

$("body").on('focus', 'select', function(e) {
    if (disableInputs) {
        e.stopPropagation();
        $(this).blur();
    }
});

function updateLayoutHeight() {
    var height = screen.height;
    
    if (device.version < 7.0) {
        height -= 20;
    }
    else {
        height -= 18;
        
        var mvp = document.getElementById('myViewport');
		mvp.setAttribute("content", "user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, target-densitydpi=device-dpi, height=" + height);
    }
    
    $("body").css("height", height + "px");
    $(".step").css("min-height", (height - 70) + "px");
    $(".overlay").css("min-height", (height - 70) + "px");
    $("#loader").css("height", height + "px");
    $("#start").css("height", height + "px");
}

function deviceReady() {
    functions.showLoader();
    
    updateLayoutHeight();
    
    app.currentOverlay = "start";
    app.currentOverlayShowing = true;

    app.currentStepIndex = 0;
    app.currentSubStepIndex = 0;
    
    app.currentStep = app.steps[app.currentStepIndex];
    app.currentSubStep = app.currentStep.subSteps[app.currentSubStepIndex];
    
    app.currentStepName = app.currentSubStep.name;
    
    disableNavButtons(1);
    
    app.storage.checkForSavedCards(function(cardsExists) {
        if (cardsExists === true) {
            app.firstCard = false;
            
            $("#nav_home_btn").data("disabled", false);
            
            getLastCards();
        }
        else {
            app.firstCard = true;
            
            $("#nav_home_btn").data("disabled", true);
            
            app.currentStepIndex = 1;
            app.currentSubStepIndex = 0;
            
            app.currentStep = app.steps[app.currentStepIndex];
            app.currentSubStep = app.currentStep.subSteps[app.currentSubStepIndex];
            
            app.currentStepName = app.currentSubStep.name;
        }
    });
    
    showOverlay(app.currentOverlay);
    
    app.storage.getUser(function(user) {
        app.user = user;
    });
    
    app.card = new Card();
    
    compileHandlebarsTemplates();
    
    var today = new Date();
    
    var twoDaysAhed = new Date();
    twoDaysAhed.setDate(twoDaysAhed.getDate() + 2);
    
    if (today.getMonth() != twoDaysAhed.getMonth())
        calendarUpdate(twoDaysAhed);
    else
        calendarUpdate(today);
    
    changeFont(app.card.fontId);
    changeFontColor(app.card.fontColorId);
    changeFontSize(app.card.fontSize);
    changeTextAlign(app.card.textAlign);
    
    updateText(app.card.text);
    updateAddressInfo();
    
    functions.hideLoader();
}

function compileHandlebarsTemplates() {
    HandlebarTemplates.previousCardTpl = Handlebars.compile($("#previous_card_tpl").html());
    
    HandlebarTemplates.previousCardListItemTpl = Handlebars.compile($("#previous_card_list_item_tpl").html());
    
    HandlebarTemplates.addressListItemTpl = Handlebars.compile($("#address_list_item_tpl").html());
    
    HandlebarTemplates.numberOfImagesTpl = Handlebars.compile($("#number_of_images_item_tpl").html());
    
    HandlebarTemplates.imageTpl = Handlebars.compile($("#image_tpl").html());
}

function saveInputValues(inputs) {
    inputs.each(function() {
        $(this).data("defaultValue", $(this).val());
    });
}

function resetInputValues(inputs) {
    inputs.each(function() {
        if ($(this).data("defaultValue"))
            $(this).val($(this).data("defaultValue"));
    });
}

$("input:not(.lowercase)").bind('keyup', function() {
    var box = event.target;
    var txt = $(this).val();
    var start = box.selectionStart;
    var end = box.selectionEnd;
    $(this).val(txt.replace(/^(.)|(\s|\-)(.)/g, function ($1) {
        return $1.toUpperCase();
    }));
    box.setSelectionRange(start, end);
});

$("input").bind('focus', function() {
    if (!disableInputs) {
        if (!$(this).data("defaultValue"))
            $(this).data("defaultValue", $(this).val());
        
        if ($(this).val() == $(this).data("defaultValue"))
            $(this).val("");
    }
});

$("input").bind('blur', function () {
    if ($(this).val() === "") {
        $(this).val($(this).data("defaultValue"));
    }
});

$("textarea, input").bind('keyup', function (e) {
    var char = $(this).val().charAt($(this).val().length - 1);
    
    if (char.charCodeAt(0) > 255) {
        navigator.notification.alert("Dette tegnet kan ikke benyttes.", function () {}, "");
        $(this).val($(this).val().substring(0, $(this).val().length - 1));
        e.preventDefault();
        return false;
    }
    
    if ($(this).is("textarea")) {
        var displayWarning = true;
        
        // If the user is trying to remove text with backspace or delete, don't display a message
        if (e.keyCode == 8 || e.keyCode == 46)
            displayWarning = false;
        
        if (checkIfTextFitsInTextarea($(this), displayWarning) === false) {
            disableNavButtons(2);
            
            navRightBtnDisabled = true;
            
            updateMenu();
        }
    }
});

function setCurrentSubStep(name) {
    for (var i = 0; i < app.steps.length; i++) {
        for (var j = 0; j < app.steps[i].subSteps.length; j++) {
            if (app.steps[i].subSteps[j].name == name) {
                app.currentSubStep = app.steps[i].subSteps[j];
                app.currentSubStepIndex = j;
                
                app.currentStep = app.steps[i];
                app.currentStepIndex = i;
                
                break;
            }
        }
    }
}

function navLeft(btn) {
    if (navLeftBtnDisabled === false) {
        if (app.currentOverlayShowing === false) {
            $("#nav_left_btn").css("background-position", $("#nav_left_btn").css("background-position").split(" ")[0] + " -42px");
            
            var avoidSlide = false;
            
            if (app.currentSubStep.previousStepName != null) {
                var previousStep = app.currentSubStep.previousStepName;
                
                setCurrentSubStep(previousStep);
                
                if (previousStep == "start") {
                    showOverlay("start");
                    
                    avoidSlide = true;
                }
                else if (previousStep == "number_of_images" && app.card.numberOfImages === 1) {
                    setCurrentSubStep("card");
                }
            }
            else {
                app.currentSubStepIndex--;
                
                if (app.currentSubStepIndex < 0) {                    
                    app.currentStepIndex--;
                    
                    if (app.currentStepIndex < 0) {
                        app.currentStepIndex = 0;
                        
                        avoidSlide = true;
                    }
                    
                    if (app.currentStepIndex == 0 && app.firstCard === true) {
                        app.currentStepIndex = 1;
                        app.currentSubStepIndex = 0;
                        
                        avoidSlide = true;
                        
                        showOverlay("start");
                    }
                    else {
                        app.currentSubStepIndex = app.steps[app.currentStepIndex].subSteps.length - 1;
                    }
                    
                    app.currentStep = app.steps[app.currentStepIndex];
                }
                
                app.currentSubStep = app.currentStep.subSteps[app.currentSubStepIndex];
            }
            
            if (avoidSlide === false)
                showStep(app.currentSubStep, "left", true);
        }
        else {
            hideOverlay();
        }
    }
}

function navRight(btn) {
    if (navRightBtnDisabled === false) {
        $(btn).css("background-position", $(btn).css("background-position").split(" ")[0] + " -42px");
        
        if (app.currentSubStep.nextStepName != null) {
            var nextStep = app.currentSubStep.nextStepName;
            
            setCurrentSubStep(nextStep);
            
            if (nextStep == "number_of_images") {
                if (card.numberOfImages < 2)
                    app.currentSubStep.nextStepDisabled = true;
            }
            else if (nextStep == "add_images") {
                addImage();
            }
            else if (nextStep == "edit_card") {
                app.card.filter = 0;
                
                updateEditCardMenu();
                
                cloneCard("add_images_card_container", "edit_card_container", "cloned_image_");
            }
            else if (nextStep == "write_text") {
                cloneCard("edit_card_container", "preview_card_container", "cloned2_image_");
            }
            else if (nextStep == "delivery_date") {
                cloneCard("preview_card_container", "preview_send_card_container", "cloned3_image_");
            }
            else if (nextStep == "preview_send") {
                updateBasket();
            }
        }
        else {
            app.currentSubStepIndex++;
            
            if (app.currentSubStepIndex >= app.steps[app.currentStepIndex].subSteps.length) {
                
                app.currentSubStepIndex = 0;
                
                app.currentStepIndex++;
                
                app.currentStep = app.steps[app.currentStepIndex];
            }
            
            app.currentSubStep = app.currentStep.subSteps[app.currentSubStepIndex];
        }
        
        $(".nav_btn").eq(app.currentStepIndex).data("disabled", false);
        
        showStep(app.currentSubStep, "right" ,true);
    }
}

function navLeftDown(btn) {
    if (navLeftBtnDisabled === false)
        $(btn).css("background-position", $(btn).css("background-position").split(" ")[0] + " -84px");
}

function navRightDown(btn) {
    if (navRightBtnDisabled === false)
        $(btn).css("background-position", $(btn).css("background-position").split(" ")[0] + " -84px");
}

function navBtn(btn) {
    if ($(btn).data("disabled") === false) {
        app.currentStepIndex = $(btn).index();
        app.currentSubStepIndex = 0;
        
        app.currentStep = app.steps[app.currentStepIndex];
        app.currentSubStep = app.currentStep.subSteps[app.currentSubStepIndex];
        
        showStep(app.currentSubStep, null, false);
    }
}

function disableNavButtons(startIndex) {
    $(".nav_btn").each(function () {
        if ($(this).index() > startIndex || ($(this).index() == 0 && app.firstCard === true))
            $(this).data("disabled", true);
        else
            $(this).data("disabled", false);
    });
}

function updateMenu() {
    if (app.currentSubStep.menuDisabled === false) {
        if ($("#nav_left_btn").is(":visible") === false) {
            $("#navigation div").show();
        }
        
        for (var i = 0; i < app.steps.length; i++) {
            var backgroundPositionX = $("#nav_" + app.steps[i].name + "_btn").css("background-position").split(" ")[0];
            var backgroundPositionY = 0;
            
            if ($("#nav_" + app.steps[i].name + "_btn").data("disabled") === false)
                backgroundPositionY = 70;
            
            if (app.steps[i].name == app.currentStep.name)
                backgroundPositionY = 140;
            
            $("#nav_" + app.steps[i].name + "_btn").css({
                "background-position": backgroundPositionX + " -" + backgroundPositionY + "px",
            });
            
            if (navLeftBtnDisabled)
                $("#nav_left_btn").css("background-position", $("#nav_left_btn").css("background-position").split(" ")[0] + " 0px");
            else
                $("#nav_left_btn").css("background-position", $("#nav_left_btn").css("background-position").split(" ")[0] + " -42px");
            
            if (navRightBtnDisabled)
                $("#nav_right_btn").css("background-position", $("#nav_right_btn").css("background-position").split(" ")[0] + " 0px");
            else
                $("#nav_right_btn").css("background-position", $("#nav_right_btn").css("background-position").split(" ")[0] + " -42px");
        }
    }
    else {
        $("#navigation div").hide();
    }
}

function showNavigationHeader(text) {
    $("#navigation_header").text(text).show();
}

function hideNavigationHeader() {
    $("#navigation_header").hide();
}

function showStep(step, direction, slide) {
    functions.hideKeyboard();
    
    if (app.currentOverlayShowing === true)
        hideOverlay();
    
    navRightBtnDisabled = step.nextStepDisabled;
    
    updateMenu();
    
    if (app.currentOverlayShowing === false && slide === true) {
        app.slideStep(step.name, direction);
    }
    else {
        $(".step").hide();
        
        $("#" + step.name).attr("class", "step stage-center").show();
        
        if (step.name == "write_text")
            setMaximumTextHeight(app.card.fontId, app.card.fontSizeId, app.card.fontSizePx);
        
        app.currentStepName = step.name;
    }

    if (step.message != null)
        showMessage(step.message);
    else
        hideMessage();
    
    app.verticalCenter(step.name);
}

function showOverlay(id) {
    functions.hideKeyboard();
    
    disableInputs = true;
    
    navRightBtnDisabled = true;
    
    updateMenu();
    
    app.slideOverlay(id, true);
    
    app.verticalCenter(id);
}

function hideOverlay() {
    functions.hideKeyboard();
    
    if (app.currentOverlay != "start")
        resetInputValues($("#" + app.currentOverlay + " input"));
    
    navRightBtnDisabled = app.currentSubStep.nextStepDisabled;
    
    updateMenu();
    
    app.slideOverlay(app.currentOverlay, false);
}

function showMessage(message) {
    $("#message").text(message);
    
    $("#message").slideDown("normal", function() {
    });
}

function hideMessage() {
    $("#message").text("");
    
    $("#message").slideUp("normal");
}

function cloneCard(sourceContainer, destContainer, imageId) {
    $("#" + destContainer + " .card_container").html("");
    
    var i = 1;
    
    $("#" + sourceContainer + " .card_container canvas").each(function() {
        var clonedCanvas = cloneCanvas($(this), imageId + i);
        
        var $container = $(this).parent().clone();
        
        $container.html("").append(clonedCanvas);
        
        $("#" + destContainer + " .card_container").append($container);
        
        i++;
    });
}

function cloneCanvas(canvas, id) {
    var newCanvas = $("<canvas/>");
    
    newCanvas.css({
        "width": canvas.css("width"),
        "height": canvas.css("height"),
        //"-webkit-transform": canvas.css("-webkit-transform"),
    });
    
    newCanvas.attr("id", id);
    
    var context = newCanvas[0].getContext("2d");
    
    canvas = document.getElementById(canvas.attr("id"));
    
    newCanvas[0].width = canvas.width;
    newCanvas[0].height = canvas.height;
    
    context.drawImage(canvas, 0, 0, canvas.width, canvas.height);
    
    return newCanvas;
}


$("#nav_left_btn").bind('touchstart', function() {
    navLeftDown(this);
});

$("#nav_right_btn").bind('touchstart', function() {
    navRightDown(this);
});

$("#nav_left_btn").bind('touchend', function() {
    navLeft(this);
});

$("#nav_right_btn").bind('touchend', function() {
    navRight(this);
});

$(".nav_btn").bind('touchstart', function() {
    navBtn(this);
});

$("#start_btn").bind('touchstart', function() {
    var rebateCode = $("#rebate_code").val();

    if (rebateCode !== "" && rebateCode !== "Kampanjekode (hvis du har)") {
        functions.checkRebateCode(rebateCode, app.user.deviceId, function(rebate) {
            hideOverlay();
            
            app.card.rebate = rebate;
            
            if (rebate.cardsLeft > 0) {
                navigator.notification.alert("Du kan sende " + rebate.cardsLeft + " kort GRATIS", function () {}, "Kampanjekode");
            }
            else if (rebate.layout !== 'undefined') {
                navigator.notification.alert("Kr 29,- pr kort inkludert porto i Norge", function () {}, "Prisinformasjon");
            }

            updatePaymentInfo();
            
            showStep(app.currentSubStep, "", false);
        });
    }
    else {
        app.card.rebate = null;
                     
        updatePaymentInfo();
        
        showStep(app.currentSubStep, "", false);
    }
});

$("#info_btn").bind('touchstart', function() {
                    $("#info_box").hide();
});




$(".step").bind('touchstart', function(event) {
    scrolling = false;
    
    if ($(this).css("overflow-y").length <= 0)
        event.preventDefault();
}).bind('touchmove', function(event) {
    if ($(this).css("overflow-y").length <= 0)
        event.preventDefault();
}).bind('touchend', function(event) {
    if ($(this).css("overflow-y").length <= 0)
        event.preventDefault();
});

var disableInputs = false;
