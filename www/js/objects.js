
var Step = function(name, subSteps) {
    this.name = name;
    this.subSteps = subSteps;
}

var SubStep = function(name, nextStepDisabled, nextStepName, previousStepName, message, menuDisabled) {
    this.name = name;
    this.nextStepDisabled = nextStepDisabled;
    this.nextStepName = typeof nextStepName !== 'undefined' ? nextStepName : null;
    this.previousStepName = typeof previousStepName !== 'undefined' ? previousStepName : null;
    this.message = typeof message !== 'undefined' ? message : null;
    this.menuDisabled = typeof menuDisabled !== 'undefined' ? menuDisabled : false;
}

var Card = function() {
    this.cardId = null;
    this.galleryId = null;
    this.orderId = null;
    this.ordered = false;
    this.orderedDate = null;
    this.images = new Array();
    this.imagesCode = null;
    this.cardType = null;
    this.numberOfImages = null;
    this.filter = 0;
    this.frameId = 0;
    this.frameRotated = 0;
    this.fontId = 53;
    this.fontSizeId = 3;
    this.fontSize = 12;
    this.fontSizePx = 16;
    this.fontColorId = 56;
    this.textAlign = 0;
    this.text = "";
    this.address = new Array();
    this.deliveryDate = null;
    this.price = 20;
    this.rebate = null;
}

var User = function(deviceId) {
    this.userId = null;
    this.deviceId = deviceId;
    this.firstName = null;
    this.lastName = null;
    this.email = null;
    this.hasSavedCreditCard = null;
}

var ImageObject = function() {
    this.imageURI = null;
    this.imageNumber = 1;
    this.canvasWidth = 0;
    this.canvasHeight = 0;
    this.posX = 0;
    this.posY = 0;
    this.editCanvasWidth = 0;
    this.editCanvasHeight = 0;
    this.frameWidth = 0;
    this.frameHeight = 0;
    this.frameOffsetX = 0;
    this.frameOffsetY = 0;
    this.offsetX = null;
    this.offsetY = null;
    this.rotateAngle = 0;
    this.zoomAmount =  1.0;
    this.slider = null;
}

var HandlebarTemplates = {};

var colors = {
    56: "#000",
    57: "#8c8c8c",
    50: "#e32d22",
    51: "#ff9600",
    52: "#17a300",
    53: "#007ccb",
    54: "#9900cb",
    55: "#fd0089",
}

var fontSizeIds = {
    10: 2,
    11: 23,
    12: 3,
    14: 4,
    16: 5,
    18: 6,
    20: 7,
}

// font id, font size id, rows, total height, font size in px
var fontRows = {
    3: {
        2: [25, 414, 14],
        3: [19, 404, 18],
        4: [17, 402, 20],
        5: [15, 424, 24],
        6: [13, 416, 27],
        7: [12, 414, 30]
    },
    
    6: {
        4: [14, 414, 23],
        5: [12, 414, 27],
        6: [11, 420, 30],
        7: [10, 420, 33]
    },
    
    5: {
        4: [15, 424, 21],
        5: [13, 402, 24],
        6: [11, 408, 28],
        7: [10, 409, 31]
    },
    
    9: {
        2: [23, 408, 14],
        3: [19, 404, 18],
        4: [17, 402, 21],
        5: [15, 424, 25],
        6: [13, 416, 28],
        7: [12, 427, 31]
    },
    
    50: {
        4: [15, 424, 24],
        5: [12, 401, 27],
        6: [11, 408, 31],
        7: [10, 409, 34]
    },
    
    53: {
        2: [15, 424, 14],
        23: [12, 401, 16],
        3: [12, 414, 17],
        4: [10, 420, 21],
        5: [9, 424, 24],
        6: [8, 438, 27],
        7: [7, 424, 30]
    },
    
    48: {
        2: [22, 415, 15],
        3: [18, 404, 19],
        4: [14, 414, 23],
        5: [13, 416, 26],
        6: [11, 408, 30],
        7: [10, 409, 33]
    },
}

var textAligns = {
    0: "left",
    1: "center",
    2: "right",
}