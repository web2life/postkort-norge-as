var Storage = function() {
    this.saveCard = function(card, callback) {
        var cards;
        
        if (window.localStorage.getItem("cards") === null)
            cards = [];
        else
            cards = JSON.parse(window.localStorage.getItem("cards"));
        
        cards.push(card);
        
        window.localStorage.setItem("cards", JSON.stringify(cards));
    }
    
    this.saveLastCardNotOrdered = function(card) {
        window.localStorage.setItem("lastCardNotOrderd", JSON.stringify(card));
    }
    
    this.getLastCardNotOrdered = function(callback) {
        var card;
        
        if (window.localStorage.getItem("lastCardNotOrderd") === null) {
            card = null;
        }
        else {
            card = JSON.parse(window.localStorage.getItem("lastCardNotOrderd"));
        }
        
        callLater(callback, card);
    }
    
    this.removeLastCardNotOrdered = function() {
        window.localStorage.removeItem("lastCardNotOrderd");
    }
    
    this.getCardById = function(cardId, callback) {
        var cards = JSON.parse(window.localStorage.getItem("cards"));
        var card = null;
        var l = cards.length;
        
        for (var i = 0; i < l; i++) {
            if (cards[i].cardId == cardId) {
                card = cards[i];
                break;
            }
        }
        
        callLater(callback, card);
    }
    
    this.getCards = function(callback) {
        callLater(callback, JSON.parse(window.localStorage.getItem("cards")));
    }
    
    this.checkForSavedCards = function(callback) {
        var cardsExists = true;
        
        if (window.localStorage.getItem("cards") === null)
            cardsExists = false;
        
        callLater(callback, cardsExists);
    }
    
    this.saveUser = function(user, callback) {
        window.localStorage.setItem("user", JSON.stringify(user));
    }
    
    this.getUser = function(callback) {
        var user;
        
        if (window.localStorage.getItem("user") === null) {
            user = new User(device.uuid);
            
            this.saveUser(user);
        }
        else {
            user = JSON.parse(window.localStorage.getItem("user"));
        }

        callLater(callback, user);
    }

    // Used to simulate async calls. This is done to provide a consistent interface with stores (like WebSqlStore)
    // that use async data access APIs
    var callLater = function(callback, data) {
        if (callback) {
            setTimeout(function() {
                callback(data);
            });
        }
    }
}