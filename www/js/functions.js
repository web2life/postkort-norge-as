var locationUrl = "http://posten.dinavykort.se/mobile_v2";
var finishUrl = locationUrl + "/finish_opt.php";
var checkRebateCodeUrl = locationUrl + "/checkRebateCode.php";
var uploadImageUrl = locationUrl + "/uploadImage.php";
var createCardUrl = locationUrl + "/createCard.php";
var preparePaymentUrl = locationUrl + "/preparePayment.php";
var paymentUrl = locationUrl + "/payment/payment.php";
var sendCardUrl = locationUrl + "/sendCard.php";
var searchUrl = locationUrl + "/searchAddress.php";

var imagesIndex = 0;

var colorCtxs = [];

//window.onerror = function(err,fn,ln) {alert("ERROR:" + err + ", " + fn + ":" + ln);};

var scrolling = false;

$("div, ul").scroll(function(){ scrolling = true; });

var functions = {
    keyboardShowing: false,
    
    hideKeyboard: function() {
        if (this.keyboardShowing === true) {
            setTimeout(function() {
                document.activeElement.blur();
                $("input").blur();
        
                this.keyboardShowing = false;
            }, 300);
        }
    },

    showLoader: function(text) {
        $("#loader_text span").text("" + text);
        
        $("#loader").show();
    },
    
    hideLoader: function() {
        $("#loader").hide();
    },

    serverNotResponding: function() {
        navigator.notification.alert("Det oppnås ikke kontakt med databasen. Vennligst prøv på nytt.", function() {}, "Kommunikasjonsfeil");
        
        this.hideLoader();
    },
    
    checkRebateCode: function(code, deviceId, callback) {
        var self = this;
        
        var dataString = "?rebateCode=" + code + "&deviceId=" + deviceId;
        
        self.showLoader(" ");
        
        $.ajax({
            type: "GET",
            url: checkRebateCodeUrl + dataString,
            processData: false,
            timeout: 30000,
            success: function(data) {
                var response = $.parseJSON(data);
               
                if (response.success === true) {
                    self.hideLoader();

                    if (typeof callback == 'function')
                        callback.call(this, response.data);
                }
                else if (response.success === false) {
                    navigator.notification.alert(response.msg, function() {}, response.title);
                    
                    self.hideLoader();
                }
                else {
                    //if (response.mysqlError)
                      //  console.log(response.mysqlError);
                    
                    self.serverNotResponding();
                }
            },
            error: function(request, status, error) {
                self.serverNotResponding();
            }
        });
    },

    uploadImage: function(imageObject, imagesCode, callback) {
        var self = this;
        
        console.log('UploadImage started - JP');
        
        var win = function(r) {
            console.log(r.response);
            var response = $.parseJSON(r.response);
            
            if (response.success === true) {
                console.log("image uploaded");
                
                if (typeof callback == 'function')
                    callback.call(this, response.data);
            }
            else {                
                navigator.notification.alert("Opplasting feilet! Vennligst prøv på nytt.", function () {}, "Opplasting feilet!");
                
                self.hideLoader();
            }
        }
        
        var fail = function(error) {
            navigator.notification.alert("Opplasting feilet! Vennligst prøv på nytt.", function () {}, "Opplasting feilet!");
            
            self.hideLoader();
        }
        
        var options = new FileUploadOptions();
        options.fileKey = "file";
        options.fileName = imageObject.imageURI.substr(imageObject.imageURI.lastIndexOf('/') + 1);
        options.mimeType = "image/jpeg";
        
        var params = {
            imageNumber: imageObject.imageNumber,
            canvasWidth: imageObject.canvasWidth,
            canvasHeight: imageObject.canvasHeight,
            editCanvasWidth: imageObject.editCanvasWidth,
            editCanvasHeight: imageObject.editCanvasHeight,
            frameWidth: imageObject.frameWidth,
            frameHeight: imageObject.frameHeight,
            frameOffsetX: imageObject.frameOffsetX,
            frameOffsetY: imageObject.frameOffsetY,
            rotateAngle: imageObject.rotateAngle,
        };
        
        if (imagesCode !== null)
            params.imagesCode = imagesCode;
            
        options.params = params;
        console.log('upload imageURL: ' + uploadImageUrl);
        console.log('imageURI: ' + imageObject.imageURI);
        var ft = new FileTransfer();
        ft.upload(imageObject.imageURI, encodeURI(uploadImageUrl), win, fail, options);
    },
        
    uploadImageByUrl: function(imageObject, imagesCode, callback) {
        var self = this;
        
        var params = {
            imageUrl: imageObject.imageURI,
            imageNumber: imageObject.imageNumber,
            canvasWidth: imageObject.canvasWidth,
            canvasHeight: imageObject.canvasHeight,
            editCanvasWidth: imageObject.editCanvasWidth,
            editCanvasHeight: imageObject.editCanvasHeight,
            frameWidth: imageObject.frameWidth,
            frameHeight: imageObject.frameHeight,
            frameOffsetX: imageObject.frameOffsetX,
            frameOffsetY: imageObject.frameOffsetY,
            rotateAngle: imageObject.rotateAngle,
        };
        
        if (imagesCode !== null)
            params.imagesCode = imagesCode;
        
        var dataString = "imageObject=" + JSON.stringify(params) + "&imageWihtUrl=true";
        

        $.ajax({
            type: "POST",
            url: uploadImageUrl,
            data: dataString,
            processData: false,            
            timeout: 120000,
            success: function(data) {
               var response = $.parseJSON(data);
                
                if (response.success === true) {
                    if (typeof callback == 'function')
                        callback.call(this, response.data);
                }
                else if (response.success === false) {
                    navigator.notification.alert(response.msg, function() {}, response.title);
                    
                    self.hideLoader();
                }
                else {
                    //if (response.mysqlError)
                        //alert(response.mysqlError);
                    
                    self.serverNotResponding();
                }
            },
            error: function(request, status, error) {
                self.serverNotResponding();
            },
        });
    },
    
    uploadImages: function(images, imagesCode, callback) {
        var self = this;
        
        console.log('uploadImages started - JP');
        this.showLoader("Laster opp postkortet");
        
        if (images[imagesIndex].imageURI.match(/http:\/\/posten.dinavykort.se\/gallery/i)) {
            this.uploadImageByUrl(images[imagesIndex], imagesCode, function(data) {
                console.log('UploadImageByUrl callback - JP');
                if (imagesIndex == images.length - 1) {
                    imagesIndex = 0;
                    
                    if (typeof callback == 'function') {
                        callback.call(this, data);
                    }
                }
                else {
                    imagesIndex++;
                    
                    self.uploadImages(images, data.imagesCode, callback);
                }
            });
        }
        else {
            this.uploadImage(images[imagesIndex], imagesCode, function(data) {

                console.log('UploadImage callback - JP  ');

                if (imagesIndex == images.length - 1) {
                    imagesIndex = 0;
                    
                    if (typeof callback == 'function') {
                        callback.call(this, data);
                    }
                }
                else {
                    imagesIndex++;
                    
                    self.uploadImages(images, data.imagesCode, callback);
                }
            });
        }
    },
    
    createCard: function(card, user, callback) {
        var self = this;
        
        var dataString = "card=" + encodeURIComponent(JSON.stringify(card)) + "&user=" + encodeURIComponent(JSON.stringify(user));

        self.showLoader("");

        $.ajax({
            type: "POST",
            url: createCardUrl,
            data: dataString,
            processData: false,
            cache: false,
            timeout: 120000,
            success: function(data) {
                var response = $.parseJSON(data);
               
                if (response.success === true) {
                    if (typeof callback == 'function')
                        callback.call(this, response.data);
                }
                else if (response.success === false) {
                    navigator.notification.alert(response.msg, function() {}, response.title);
               
                    self.hideLoader();
                }
                else {
                    //if (response.mysqlError)
                        //alert(response.mysqlError);
                    
                    self.serverNotResponding();
                }
            },
            error: function(request, status, error) {
                self.serverNotResponding();
            },
        });
    },
    
    uploadCard: function(card, user, callback) {
        var self = this;
        console.log('Upload card started - JP');
        this.uploadImages(card.images, card.imagesCode, function(data) {
            card.imagesCode = data.imagesCode;
            
            self.createCard(card, user, function(data) {
                card.cardId = data.cardId;
                card.galleryId = data.galleryId;
                user.userId = data.userId;

                if (typeof callback == 'function')
                    callback.call(this, card, user);
            });
        });
    },
        
    preparePayment: function(card, user, callback) {
        var dataString = "card=" + encodeURIComponent(JSON.stringify(card)) + "&user=" + encodeURIComponent(JSON.stringify(user));

        var self = this;
        
        $.ajax({
            type: "POST",
            url: preparePaymentUrl,
            data: dataString,
            processData: false,
            cache: false,
            timeout: 120000,
            success: function(data) {
                var response = $.parseJSON(data);
                
                if (response.success === true) {
                    if (typeof callback == 'function')
                        callback.call(this, response.data);
                }
                else if (response.success === false) {
                    navigator.notification.alert(response.msg, function() {}, response.title);
                    
                    self.hideLoader();
                }
                else {
                    //if (response.mysqlError)
                        //alert(response.mysqlError);

                    self.serverNotResponding();
                }
            },
            error: function(request, status, error) {
                self.serverNotResponding();
            },
        });
    },
    
    sendCard: function(card, user, callback) {
        var dataString = "card=" + encodeURIComponent(JSON.stringify(card)) + "&user=" + encodeURIComponent(JSON.stringify(user));
        
        var self = this;
        
        $.ajax({
            type: "POST",
            url: sendCardUrl,
            data: dataString,
            processData: false,
            cache: false,
            timeout: 120000,
            success: function(data) {
                var response = $.parseJSON(data);
                
                if (response.success === true) {
                    if (typeof callback == 'function')
                        callback.call(this, response.data);
                }
                else if (response.success === false) {
                    navigator.notification.alert(response.msg, function() {}, response.title);
                    
                    self.hideLoader();
                }
                else {
                    //if (response.mysqlError)
                        //alert(response.mysqlError);
                    
                    self.serverNotResponding();
                }
            },
            error: function(request, status, error) {
                self.serverNotResponding();
            },
        });
    },
    
    searchAddress: function(firstName, lastName, city, phone, callback) {
        var self = this;
        
        this.showLoader("Søker");
        
        var dataString = searchUrl + "?firstname=" + firstName + "&lastname=" + lastName + "&city=" + city + "&phone=" + phone;
        
        $.ajax({
            type: "GET",
            timeout: 30000,
            url: dataString,
            processData: false,
            success: function (data) {
                if (data != "" && data != null) {
                    var response = $.parseJSON(data);
                    
                    if (response != null) {
                        var searchResult = new Array();
                        
                        var items = "";
                        
                        $.each(response, function(key, val) {
                            var addressArray = val.split('--');
                            
                            var address = {
                                name: addressArray[0],
                                street: addressArray[1],
                                postCode: addressArray[2],
                                class: "add_address_item",
                            };
                            
                            searchResult[key] = address;
                            
                            var item = HandlebarTemplates.addressListItemTpl(address);

                            items += item;
                        });
                        
                        if (typeof callback == 'function')
                            callback.call(this, searchResult, items);
                    }
                    else {
                        navigator.notification.alert("Null treff", function () {}, "Søk etter mottaker");
                        
                        self.hideLoader();
                    }
                }
                else {
                    navigator.notification.alert("Null treff", function () {}, "Søk etter mottaker");
                    
                    self.hideLoader();
                }
            },
            error: function(request, status, error) {
                navigator.notification.alert("Null treff", function () {}, "Søk etter mottaker");
                
                self.hideLoader();
            }
        });
    },
    
    checkInternetConnection: function() {
        var self = this;
        
        var networkState = navigator.connection.type;
        
        var states = {};
        states[Connection.UNKNOWN] = 'Unknown connection';
        states[Connection.ETHERNET] = 'Ethernet connection';
        states[Connection.WIFI] = 'WiFi connection';
        states[Connection.CELL_2G] = 'Cell 2G connection';
        states[Connection.CELL_3G] = 'Cell 3G connection';
        states[Connection.CELL_4G] = 'Cell 4G connection';
        states[Connection.NONE] = 'No network connection';
        
        if ((states[networkState] == "No network connection") || (states[networkState] == "Unknown connection")) {
            navigator.notification.alert("Slå på 3G eller WIFI og prøv igjen.", function () { self.checkInternetConnection(); }, "Ingen internett-tilkobling");
        }
    },
    
    openExternalLink: function(link) {
         window.open(link, "_blank", "location=yes");
    },
    
    saveColorCanvas: function(canvasId, index) {
        var colorCanvas = document.createElement('canvas');
        
        var canvas = document.getElementById(canvasId);
        
        var ctx = canvas.getContext("2d");
        
        colorCanvas.setAttribute("width", canvas.width);
        colorCanvas.setAttribute("height", canvas.height);
        
        colorCtx = colorCanvas.getContext("2d");
        
        var imgPixels = ctx.getImageData(0, 0, canvas.width, canvas.height);
        
        colorCtx.putImageData(imgPixels, 0, 0);
        
        colorCtxs[index] = colorCtx;
    },
    
    createGrayscale: function(canvasId, callback) {
        var canvas = document.getElementById(canvasId);
        
        var ctx = canvas.getContext("2d");
        
        var imgGrayPixels = ctx.getImageData(0, 0, canvas.width, canvas.height);
        
        var imgData = imgGrayPixels.data;
        
        for(var y = 0; y < imgGrayPixels.height; y++){
            for(var x = 0; x < imgGrayPixels.width; x++){
                var i = (y * 4) * imgGrayPixels.width + x * 4;
                var avg = (imgData[i] + imgData[i + 1] + imgData[i + 2]) / 3;
                imgData[i] = avg;
                imgData[i + 1] = avg;
                imgData[i + 2] = avg;
            }
        }
        
        ctx.putImageData(imgGrayPixels, 0, 0);
        
        if (typeof callback == 'function')
            callback.call(this);
    },
    
    resetColorCanvas: function(canvasId, index) {
        var canvas = document.getElementById(canvasId);
        
        var ctx = canvas.getContext("2d");
        
        var colorCtx = colorCtxs[index];
        
        var imgPixels = colorCtx.getImageData(0, 0, canvas.width, canvas.height);
        
        ctx.putImageData(imgPixels, 0, 0);
    },
}

function checkIfTextFitsInTextarea(textarea, displayWarning) {
    if (textarea[0].scrollHeight > textarea.height()) {
        if (displayWarning === true)
            navigator.notification.alert("NB! Hele din hilsen får ikke plass på kortet! Gjør skrifttypen mindre, prøv en annen skrifttype eller endre teksten.", function () {}, " ");
        
        if (app.currentSubStep.name == "write_text") {
            disableNavButtons(2);
        
            navRightBtnDisabled = true;
        
            updateMenu();
        }
        
        return false;
    }
    else {
        if (app.currentSubStep.name == "write_text") {            
            navRightBtnDisabled = false;
            
            updateMenu();
        }
        
        return true;
    }
}

function setMaximumTextHeight(fontId, fontSizeId, fontSize) {    
    $("#text_rows").css({
        "font-family": "F" + fontId,
        "font-size": fontSize + "px",
    });
    
    var rows = fontRows[fontId][fontSizeId][0];

    var html = "";
    
    for (var i = 1; i <= rows; i++) {
        var row = i;
        
        if (i != rows)
            row += "<br/>";
        
        html += row;
    }
    
    $("#text_rows").html(html);
    
    var lineHeightOrg = fontRows[fontId][fontSizeId][1] / rows;

    lineHeight = Math.ceil(($("#write_text_card_background").height() - 20) / (463 / lineHeightOrg));
    lineHeightPreview = Math.ceil(213 / (463 / lineHeightOrg));
    
    $("#text_rows").css("line-height", lineHeight + "px");
    
    var height = $("#text_rows").height();
    
    if (height > 0) {
        $("#text").height($("#text_rows").height());
        
        $("#text").css("line-height", lineHeight + "px");
        $("#preview_text").css("line-height", lineHeightPreview + "px");
    }

}

function changeFont(fontId, callback) {
    if (fontId != 53 && app.card.fontSize == 11) {
        changeFontSize(12, function() {
                       changeFont(fontId, callback);
                       });
    }
    else if ((fontId == 5 || fontId == 6 || fontId == 50) && app.card.fontSize < 14) {
        changeFontSize(14, function() {
                       changeFont(fontId, callback);
                       });
    }
    else {
        $("#text").css("font-family", "F" + fontId);
        $("#preview_text").css("font-family", "F" + fontId);
        
        if (fontId == 6 || fontId == 5)
            $("#text").css("word-spacing", "7px");
        else if (fontId == 50)
            $("#text").css("word-spacing", "8px");
        else            
            $("#text").css("word-spacing", "normal");
        
        app.card.fontId = fontId;
        
        if (app.currentSubStep.name == "write_text") {
            setMaximumTextHeight(app.card.fontId, app.card.fontSizeId, app.card.fontSizePx);
            
            checkIfTextFitsInTextarea($("#text"), true);
        }
        
        if (typeof callback == 'function')
            callback.call(this);
    }
}

function changeFontColor(colorId) {
    app.card.fontColorId = colorId;
    
    $("#text").css("color", colors[colorId]);
    $("#preview_text").css("color", colors[colorId]);
}

function changeFontSize(size, callback) {
    // Fontsize conversion to px based on the ration 0.75:1
    fontSizeInPx = size / 0.75;
    
    // Scale the fonsize in relation to the height of the card in the app and the height the printed card in px
    fontSize = Math.round(($("#write_text_card_background").height() - 20) / (397 / fontRows[app.card.fontId][fontSizeIds[size]][2]));
    fontSizePreview = Math.round(213 / (397 / fontRows[app.card.fontId][fontSizeIds[size]][2]));
    
    $("#text").css("font-size", fontSize + "px");
    $("#preview_text").css("font-size", fontSizePreview + "px");
    
    // save font size in pt
    app.card.fontSize = size;
    // save font size in px
    app.card.fontSizePx = fontSize;
    
    app.card.fontSizeId = fontSizeIds[size];
    
    if (app.currentSubStep.name == "write_text") {
        setMaximumTextHeight(app.card.fontId, app.card.fontSizeId, app.card.fontSizePx);
        
        checkIfTextFitsInTextarea($("#text"), true);
    }
    
    if (typeof callback == 'function')
        callback.call(this);
}

function updateFontSize(effect) {
    var size = app.card.fontSize;
    
    if (effect === "increase") {
        if (app.card.fontId == 53 && (size == 10 || size == 11))
            size++;
        else
            size = size + 2;
        
        if (size > 20)
            size = 20;
    }
    else {
        if (app.card.fontId == 53 && (size == 11 || size == 12))
            size--;
        else
            size = size - 2;
        
        if (size < 10)
            size = 10;
    }
    
    if ((app.card.fontId == 5 || app.card.fontId == 6 || app.card.fontId == 50) && size < 14)
        size = 14;

    changeFontSize(size);
}



function changeTextAlign(alignId) {
    app.card.textAlign = alignId;
    
    $("#text").css("text-align", textAligns[alignId]);
    $("#preview_text").css("text-align", textAligns[alignId]);
}

function updateText(text) {
    $("#text").val(text);
    
    $("#preview_text").text(text).html(function (index, old) {
        return old.replace(/\n/g, "<br />")
    });
    
    app.card.text = text;
    
    if ($("#text")[0].scrollHeight > $("#text").height()) {
        navigator.notification.alert("NB! Hele din hilsen får ikke plass på kortet! Gjør skrifttypen mindre, prøv en annen skrifttype eller endre teksten.", function () {}, " ");
    }
}
